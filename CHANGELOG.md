# Changelog for uri-resolver

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.10.1]

- Geoportal Exporter: added static pages as freemarker templates
- Bug fixing: `createLink` method in the GeoportalExporter service [#28641]
- Bug fixing: `export` method in the GeoportalExporter service [#28580]

## [v2.10.0] - 2024-12-02

- GeoPortal-Resolver enhancement: implemented share link towards Geoportal Data-Entry facility [#27135]
- Added Geoportal Exporter as PDF [#27274]
- Migrated to `catalogue-util-library` [#27423]
- Included new SHUB client library [#27944]
- Moved to `gcube-smartgears-bom 2.5.1{-SNAPSHOT}` [#28026]
- Bug fixing StorageID resolver [#28276]
- Added HealthCheckResponse via `microprofile-health-api` (see https://microprofile.io/specifications/microprofile-health/)

## [v2.9.0] - 2023-04-26

- GeoPortal-Resolver implemented [#24792]

## [v2.8.1] - 2022-06-13

- [#23156] CatalogueResolver enhanced to manage (create/resolve) an input query string
- [#23495] Removed the check on the gcube token stored in thread local. The working context of VRE will be checked from ScopeProvider


## [v2.7.3] - 2022-04-27

- [#23213] Fixed NoClassDefFoundError: org/geotoolkit/xml/XML

## [v2.7.2] - 2022-04-06

- [#23113] Implemented a new legacy-token resolver interface
- [#23049] Integrated Enunciate engine for Java Web service API (https://github.com/stoicflame/enunciate/)
- [#20743] Integrated JavaMelody 
- [#23105] Added D4Science brand in the APIs Documentation pages
- [#22757] Moved to storagehub-client-library 2.0.0
- [#20743] Integration with javamelody

## [v2.6.1] - 2022-01-17

- [#21411] add forceClose method from storage-manager, used for closing mongodb connections related to the old https urls (managed by storage-manager)
- migrated to storage-manager3
- [#21560] Checking the bug fix done
- Moved to gcube-smartgears-bom.2.1.0

## [v2.5.0] - 2021-04-08

- [#20993] Supported new resource "Wekeo Interface" - gettoken.
- [#21093] StorageHubResolver HEAD request does not support Content-Length

## [v2.4.1] - 2021-01-13

- [#19942] Fixing master build fails

## [v2.4.0] - 2020-06-18

- [#18967] Extend the URIResolver behaviour to deal with Catalogue items belonging to Dismissed VREs

## [v2.3.2] - 2020-05-18

- [#19288] Release the URI-Resolver to integrate the patched Uri-Resolver-Manager


## [v2.3.1] - 2020-04-06

- [Bug #18951] URI-Resolver: redirect to catalogue public item must manage the case 'catalogue-'


## [v2.2.0] - 2019-11-12

- [Feature #18038] Catalogue Resolver: resolve a public ITEM URL to the public VRE Catalogue (if available)


## [v2.2.0] - 2019-10-04

- [Feature #17630] Support parametric Content-Disposition
- [Bug #17650] Bug #17650: URI-Resolver: HEAD request to StorageID link does not return the "content-disposition

## [v2.1.1] - 2019-07-16

- [Incident #17180] Bug fixes

## [v2.1.0] - 2019-03-14

- [Feature #16263] Added new dataminer-invocation-model
- [Task #16296] Bug fixes
- [Task #16471] Tested Catalogue Resolver backward compatibility
- [Incident #16671] Fixing Catalogue Resolver in case of working with INFRASTRUCTURE and VO scope
- [Incident #16713] GN harvesting from CKAN does not work properly

## [v2.0.0] - 2018-10-23

- [Task #12740] Developed as web-service by using jersey framework
- [Task #12294] Created the resolver 'parthenosregistry'
- [Task #13006] Fixing issue on resolving public Catalogues
- [Task #12969] Create the new resolvers: "Analitycs" and "Knime"
- [Feature #13072] Provide Metadata of a public link

## [v1.15.0] - 2018-07-20

- [Task #10070] Catalogue Resolver enhancement: resolve public/private items to public/private catalogue

## [v1.14.0] - 2017-09-06

- [Task #9538] Geonetwork-Uri Resolver enhancement: provide new filters and improve current ones

## [v1.13.0] - 2017-07-03

- [Feature #9108] Edit the GenericResource (ApplicationProfile-Gis Viewer Application)

## [1-12-0] - 2017-05-15

[Feature #8494] Remove GeoServices query from URI-Resolver



## [1-11-0] - [2017-03-22]

[Feature #7350] GeoExplorer Resolver: resolve a GeoExplorer Link

[Task #7626] Fix issue on set/reset scope

[Task #7807] Provide a check for HAProxy



## [1-10-0] - [2017-02-28]

[Task #6492] Catalogue Resolver: "improve"/"build better" public URLs to products

[Task #6952] Catalogue Resolver: update on the fly the Application Profile for VRE's used to resolve Product URL



## [1-9-0] - [2016-12-20]

[Task #6119] Provide CatalogueResolver: get/resolve a link to a CKAN Entity

[Task #6262] Catalogue Resolver: get/resolve a direct link



## [1-8-0] - [2016-10-26]

Removed scope provider from several resolver



## [1-7-0] - [2016-06-09]

[Feature #4207] Uri Resolver upgrade: it must support new Geonetwork Manager

[Task #4250] Geonetwork Resolver upgrade: it must return only "private" Metadata Ids for CKAN harversting



## [1-6-0] - [2016-05-16]

[Task #3135] Uri Resolver enhancements: create a Geonetwork Resolver

[Feature #4000] URI Resolver - must support HEAD request



## [1-5-0] - [2016-03-17]

[Feature #2008] Updated the method to resolve gCube Storage ID

[Feature #1925] Added class UriResolverRewriteFilter to filter the different public link types, see: #1959

[gCube - Support #2695] Uri Resolver: patch to fix old bug in HL renaming files



## [1-4-0] - [2015-09-21]

[Feature #416] Added code to read fileName and content-type (mime type) from Storage



## [1-3-0] - [2015-04-28]

Fixed bug on encoding

Added new resolver: Storage ID Resolver



## [1-2-0] - [2014-10-20]

Integrated Gis-Resolver



## [1-1-0] - [2013-07-09]

Added fileName and contentType parameters to SMP URI resolver



## [v1.0.0] - 2013-04-19

- First release

