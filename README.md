# gCube URI Resolver

The URI Resolver is a RESTful service which gives access via HTTP(s) to different gCube Resolver services and gCube Applications.

The URI Resolver production service is available at [D4Science URI Resolver](https://data.d4science.org/)

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

You can find the URI Resolver documentation at [URI Resolver Wiki Page](https://wiki.gcube-system.org/gcube/URI_Resolver)

The API documentation accessible by this [link](https://wiki.gcube-system.org/gcube/URI_Resolver#Instances) is provided by [Enunciate](https://github.com/stoicflame/enunciate/) 

## Change log

See the [URI Resolver Releases](https://code-repo.d4science.org/gCubeSystem/uri-resolver/releases)

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://infrascience.isti.cnr.it)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


