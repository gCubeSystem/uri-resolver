package catalogue;

/**
 *
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.junit.Test;

import resources.GcoreEndpointReader;
import resources.GetAllInfrastructureScopes;

/**
 * The Class CatalogueMatching_SCOPE_CKANCONNECTOR.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 7, 2022
 */
public class CatalogueMatching_SCOPE_CKANCONNECTOR {

	// GN CONFIGURATIONS
	static String rootScope = "/d4science.research-infrastructures.eu";
	// static String rootScope = "/pred4s";
//	static String rootScope = "/gcube";
	static LoginLevel loginLevel = LoginLevel.CKAN;

	static PrintWriter reportPrintWriter;
	static PrintWriter errorPrintWriter;
	
	private static int c = 0;

	/**
	 * Test catalogue discovery.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testCatalogueDiscovery() throws Exception {

		try {
			FileWriter reportWriter = new FileWriter(rootScope.substring(1, rootScope.length()) + "_report_"
					+ CatalogueMatching_SCOPE_CKANCONNECTOR.class.getSimpleName() + ".csv", true);
			FileWriter errorWriter = new FileWriter(rootScope.substring(1, rootScope.length()) + "_error_"
					+ CatalogueMatching_SCOPE_CKANCONNECTOR.class.getSimpleName() + ".csv", true);
			BufferedWriter reportBW = new BufferedWriter(reportWriter);
			BufferedWriter errorBW = new BufferedWriter(errorWriter);
			reportPrintWriter = new PrintWriter(reportBW);
			reportPrintWriter.println("NB.; SCOPE; CKAN_CONNECTOR_URL;");
			errorPrintWriter = new PrintWriter(errorBW);
			errorPrintWriter.println("NB.; SCOPE; ERROR MESSAGE;");
		} catch (Exception e) {
			e.printStackTrace();
		}

//		final Path destination = Paths.get("report_matching_gn_catalogue.csv");
//		Files.w(re, destination);

		List<String> scopesProd = new ArrayList<String>();

		// RuntimeResourceReader readerRR;
		try {
			// MODE-1
//			readerRR = new RuntimeResourceReader(rootScope, platformName, null);
//			System.out.println(readerRR.toString());
//			System.out.println("Using GN: "+readerRR.getParameters());
//			System.out.println("Scopes are: "+readerRR.getScopes().size());
//			scopesProd.addAll(readerRR.getScopes());

			// MODE-2
			Map<String, String> mapScopes = GetAllInfrastructureScopes.loadMapOfScopeNameToFullScope(rootScope);
			scopesProd.addAll(mapScopes.values());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Read " + scopesProd.size() + " VREs from root scope: " + rootScope);
		for (String vre : scopesProd) {
			System.out.println("VRE: " + vre);
		}

		int count = 0;
		try {

			final String ckanResource = "org.gcube.data.access.ckanconnector.CkanConnector";
			final String serviceName = "CkanConnector";
			final String serviceClass = "DataAccess";

			int totalScope = scopesProd.size();
			int reportOp = 0;
			int errorOp = 0;
			for (String scope : scopesProd) {
				count++;
				System.out.println("#### Operation "+ count+ " of "+totalScope+") Fetching scope: " + scope);
				ScopeProvider.instance.set(scope);
				GcoreEndpointReader reader = null;
				try {
					// discovery = new GCatClientDiscovery();
					reader = new GcoreEndpointReader(scope, serviceClass, serviceName, ckanResource);
				} catch (Exception e) {
					System.out.println("gCat not instanciable in the scope: " + scope);
					errorOp++;
					writeError(errorOp + "; " + scope + " ; " + e.getMessage());
				}

				if (reader != null) {
					reportOp++;
					writeReport(reportOp + "; " + scope + " ; " + reader.getEndPoints().get(0) + " ; ");
				}
				
				System.out.println("Sleeping...");
				Thread.sleep(500);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reportPrintWriter != null)
				reportPrintWriter.close();

			if (errorPrintWriter != null)
				errorPrintWriter.close();

			System.out.println("Performed fetching from " + count + " scopes");
			System.out.println("FINISHED!!!");
		}

	}

	/**
	 * Write report.
	 *
	 * @param newline the newline
	 */
	private static synchronized void writeReport(String newline) {
		c++;
		reportPrintWriter.println(newline);
	}

	/**
	 * Write error.
	 *
	 * @param newline the newline
	 */
	private static synchronized void writeError(String newline) {
		c++;
		errorPrintWriter.println(newline);
	}

}
