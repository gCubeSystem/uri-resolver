package geoportal;

import static org.junit.Assert.fail;

import org.gcube.datatransfer.resolver.geoportal.exporter.GeoportalHTMLMessageGenerator;
import org.junit.Test;

public class GeoportalHTMLMessageGeneratorTest {

	//@Test
	public void testEntityHTMLMessage_ExceptionHandling() {
		try {
			String entity = GeoportalHTMLMessageGenerator.entityHTMLMessage("project1", "Exporting", "Please wait",
					true, "http://example.com/view.pdf");
			System.out.println("Entity: " + entity);

		} catch (RuntimeException e) {
			fail("RuntimeException thrown: " + e);
		}
	}
}
