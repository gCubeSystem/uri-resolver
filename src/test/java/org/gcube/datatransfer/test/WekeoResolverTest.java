package org.gcube.datatransfer.test;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.services.WekeoResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class WekeoResolverTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 31, 2021
 */
public class WekeoResolverTest {

	private static Logger logger = LoggerFactory.getLogger(WekeoResolverTest.class);

	private final static String RUNTIME_WKEO_RESOURCE_NAME = "WekeoDataBroker";

	public static final String scope = "/gcube/devsec/devVRE";
	
	
	//@Test
	public void testWekeo() throws Exception{
		
		StringBuilder wekeoResponse = new StringBuilder();
		try {

			logger.info(" test starts...");

			ScopeProvider.instance.set(scope);
			AccessPoint wekeoAccessPoint = WekeoResolver.readWekeoServiceEndpoint(null, scope);

			if (wekeoAccessPoint != null) {

				String wekeoUsername = wekeoAccessPoint.username();
				String wekeoAddress = wekeoAccessPoint.address();
				String wekeoPwd = wekeoAccessPoint.password();

				// printing the access point found
				if (logger.isDebugEnabled()) {
					String msg = String.format("Found the username '%s' and the address '%s' to perform the request",
							wekeoUsername, wekeoAddress);
					logger.debug(msg);
				}

				logger.info("The pwd is: "+wekeoPwd);
				// decrypting the pwd
				if (wekeoPwd != null) {
					wekeoPwd = StringEncrypter.getEncrypter().decrypt(wekeoPwd);
					logger.info("Decrypted pwd registered into Access Point '" + wekeoAccessPoint.name() + "' is: "
							+ wekeoPwd.substring(0,wekeoPwd.length()/2)+"...");
				}

				if (wekeoUsername != null && wekeoPwd != null & wekeoAddress != null) {
					HttpURLConnection connection = null;
					try {

						String authString = wekeoUsername + ":" + wekeoPwd;
						byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
						String authStringEnc = new String(authEncBytes);
						logger.debug("Base64 encoded auth string: " + authStringEnc);

						logger.info("Performing the request to: "+wekeoAddress);
						URL url = new URL(wekeoAddress);
						connection = (HttpURLConnection) url.openConnection();
						connection.setRequestMethod("GET");
						connection.setDoOutput(true);
						connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
						InputStream content = (InputStream) connection.getInputStream();
						InputStreamReader in = new InputStreamReader(content);
						
						logger.info("the response code is: "+connection.getResponseCode());
						
						int numCharsRead;
						char[] charArray = new char[1024];
						StringBuffer sb = new StringBuffer();
						logger.debug("reading the response...");
						while ((numCharsRead = in.read(charArray)) > 0) {
							sb.append(charArray, 0, numCharsRead);
						}
						wekeoResponse.append(sb.toString());
						//System.out.println(wekeoResponse);

					} catch (Exception e) {
						logger.error(e.getMessage(), e);
						String error = String.format("Error on performing request to %s", wekeoAddress);
						throw new Exception(error);
					} finally {
						try {
							if (connection != null) {
								connection.disconnect();
							}
						}catch (Exception e) {
						
						}
					}

				} else {
					String error = String.format(
							"I cannot read the configurations (adress, username,password) from %s in the scope %s",
							RUNTIME_WKEO_RESOURCE_NAME, scope);
					throw new Exception(error);
				}
			}

			// to be sure
			if (wekeoResponse.length() == 0) {
				String error = String
						.format("Sorry an error occured on getting the access token from Wekeo. Please, retry the request");
				throw new Exception(error);
			}
			
			logger.info("returning: \n"+wekeoResponse.toString());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
	}
}
