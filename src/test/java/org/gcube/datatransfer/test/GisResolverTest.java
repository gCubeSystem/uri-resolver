package org.gcube.datatransfer.test;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.gis.entity.GisLayerItem;
import org.gcube.datatransfer.resolver.services.GisResolver;

/**
 *
 */
/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Sep 7, 2016
 */
public class GisResolverTest {
	
	static String scope = "/d4science.research-infrastructures.eu/gCubeApps/BiodiversityLab";
	static String gisUUID = "6b99efdf-2202-4b6f-aaa3-7e10e0bf09f4";
	public static void main(String[] args) {
		GisResolver gisResolver = new GisResolver();
		ScopeProvider.instance.set(scope);
		//ServiceEndpointParameters geonetworkParams = getCachedServerParameters(scope);
		try {
			GisLayerItem gisLayerItem = gisResolver.getGisLayerForLayerUUID(null, scope, gisUUID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/*//TODO TO BE MOVED
	 * public void resolve(){
		GisResolver gisResolver = new GisResolver();
		String scope = "/gcube/devsec/devVRE";
		String UUID = "177e1c3c-4a22-4ad9-b015-bfc443d16cb8";
		try {
			ServiceEndpointParameters geonetworkParams = gisResolver.getCachedServerParameters(scope);
			String wmsRequest = gisResolver.getLayerWmsRequest(scope, UUID, geonetworkParams);
			System.out.println("Final url is: " + wmsRequest);
			wmsRequest = URLEncoder.encode(wmsRequest, UTF_8);
			System.out.println("Encoded WMS request is: " + wmsRequest);
			String gisPortletUrl = gisResolver.getGisViewerApplicationURL(scope);
			System.out.println("Gis Viewer Application url is: " + gisPortletUrl);
//			logger.info("WmsRequest is: " + wmsRequest);
//			wmsRequest = encodeURLWithParamDelimiter(wmsRequest);
//			logger.info("Encoded url is: " + wmsRequest);
//			wmsRequest = appendParamReplacement(wmsRequest);
			gisPortletUrl+="?wmsrequest="+wmsRequest;

			System.out.println(gisPortletUrl);
//			urlRedirect(req, resp, gisPortletUrl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
