package org.gcube.datatransfer.test;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.catalogue.resource.CkanCatalogueConfigurationsReader;
import org.gcube.datatransfer.resolver.catalogue.resource.GatewayCKANCatalogueReference;
import org.gcube.datatransfer.resolver.init.UriResolverSmartGearManagerInit;
import org.gcube.datatransfer.resolver.services.CatalogueResolver;

/**
 * The Class CatalogueResolverTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         May 13, 2019
 */
public class CatalogueResolverTest {

	private static String entityName = "testing_bug_fix_21560_as_public";
	private static String entityContext = "ctlg";
	private static String vreName = "devVRE";

	private static String rootContextScope = "/gcube/devsec/devVRE";
	private static String authorizationToken = "";

	// @Before
	public void init() {

		UriResolverSmartGearManagerInit.setRootContextScope(rootContextScope);
	}

	// @Test
	public void testCatalogueResolver() {
		ScopeProvider.instance.set(rootContextScope);
		SecurityTokenProvider.instance.set(authorizationToken);
		CatalogueResolver catalogRes = new CatalogueResolver();
		catalogRes.resolveCatalogue(null, entityName, vreName, entityContext);

	}

	// @Test
	public void testCatalogueLoadEndPoints() {

		try {

			// ScopeProvider.instance.set("/d4science.research-infrastructures.eu");
			GatewayCKANCatalogueReference ckanCatalogueReference = CkanCatalogueConfigurationsReader
					.loadCatalogueEndPoints();
			System.out.println(ckanCatalogueReference.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
