package org.gcube.datatransfer.test;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.services.exceptions.NotFoundException;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

/**
 * The Class ServiceEndpointReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jan 27, 2022
 */
public class ServiceEndpointReader {
	
	private static final String RESOURCE_NAME = "DEV Geoserver";
	private static final String CATEGORY_NAME = "Gis";
	private static final String SCOPE = "/gcube/devsec/devVRE";
	
	public static Logger LOG = Logger.getLogger(ServiceEndpointReader.class);

	
	public static void main(String[] args) {
		try {
			readServiceEndpoint(SCOPE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Read service endpoint.
	 *
	 * @param req the req
	 * @param scope the scope
	 * @throws Exception 
	 */
	public static void readServiceEndpoint(String scope) throws Exception{

		String callerScope = null;
		try{
			callerScope = ScopeProvider.instance.get();
			ScopeProvider.instance.set(scope);
			LOG.info("Searching SE "+RESOURCE_NAME +" configurations in the scope: "+ScopeProvider.instance.get());

			SimpleQuery query = queryFor(ServiceEndpoint.class);
			query.addCondition("$resource/Profile/Name/text() eq '"+ RESOURCE_NAME +"'");
			query.addCondition("$resource/Profile/Category/text() eq '"+ CATEGORY_NAME +"'");

			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
			List<ServiceEndpoint> toReturn = client.submit(query);

			LOG.info("The query returned "+toReturn.size()+ " ServiceEndpoint/s");

			if(toReturn.size()==0){
				String errorMessage = String.format("Missing the RR with Name '%s' and Category '%s' in the scope '%s'. Please contact the support.",RESOURCE_NAME,CATEGORY_NAME,ScopeProvider.instance.get());
				LOG.error(errorMessage);
				throw new Exception(errorMessage);

			}

			String accessPointUsername = null;
			String accessPointPwd = null;
			
			ServiceEndpoint se = toReturn.get(0);
			Collection<AccessPoint> theAccessPoints = se.profile().accessPoints().asCollection();
			for (AccessPoint accessPoint : theAccessPoints) {
				accessPointUsername = accessPoint.username();
				System.out.println("AccessPoint username: "+accessPointPwd);
				accessPointPwd = accessPoint.password();
				if(accessPointUsername!=null && accessPointPwd!=null) {
					LOG.info("returning the access point with name: "+accessPoint.name());
					if (accessPointPwd != null) {
						accessPointPwd = StringEncrypter.getEncrypter().decrypt(accessPointPwd);
						LOG.info("Decrypted pwd registered into Access Point '" + accessPoint.name() + "' is: "
								+ accessPointPwd+"...");
						
						System.out.println("AccessPoint pwd is: "+accessPointPwd);
					}
				}
			}
			

		}catch(Exception e){
			
			if(e instanceof NotFoundException)
				throw e;
			
			String errorMessage = "Error occurred on reading the "+RESOURCE_NAME+" SE registered in the scope: "+ScopeProvider.instance.get();
			LOG.error(errorMessage, e);
			throw new Exception(errorMessage);

		}finally{
			if(callerScope!=null){
				LOG.info("Setting to the callerScope scope: "+callerScope);
				ScopeProvider.instance.set(callerScope);
			}else{
				LOG.info("Reset scope");
				ScopeProvider.instance.reset();
			}
		}

	}

}
