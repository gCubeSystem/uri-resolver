package org.gcube.datatransfer.test;
import org.gcube.datatransfer.resolver.catalogue.resource.CatalogueStaticConfigurations;

public class CatalogueNameExtractor {

	String[] CKANs = new String[] { 
			"https://ckan-grsf-admin2.d4science.org", 
			"https://ckan.pre.d4science.org/",
			"https://ckan-aginfra.d4science.org",
			"https://catalogue.d4science.org/", 
			"https://catalogue-imarine.d4science.org/" };

	//@Test
	public void extraCatalogueName() {
		CatalogueStaticConfigurations staticConf = new CatalogueStaticConfigurations();
		
		for (int i = 0; i < CKANs.length; i++) {
			System.out.println(CKANs[i]);
			staticConf.buildRelativeURLToPublicCatalogueGateway(CKANs[i]);
			System.out.println("\n\n");
		}
	}

}
