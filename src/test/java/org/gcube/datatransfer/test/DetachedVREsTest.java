package org.gcube.datatransfer.test;
import java.util.concurrent.ExecutionException;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.caches.LoadingMapOfDetachedVRE;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DetachedVREsTest {
	
	public static Logger LOG = LoggerFactory.getLogger(DetachedVREsTest.class);
	
	public static String TOKEN = "";
	
	public static String detachedVREName = "BlueBridgeProject";
	
	public static String rootScope = "/gcube";
	
	
	//@Before
	public void initCache() {
		
	}
	/**
	 * Storage hub test.
	 *
	 * @throws Exception the exception
	 */
	//@Test
	public void testCacheOfDetachedVREs() throws Exception{

		try {
			ScopeProvider.instance.set(rootScope);
			SecurityTokenProvider.instance.set(TOKEN);
			//LoadingMapOfDetachedVRE cache = new LoadingMapOfDetachedVRE();
			VRE theDetachedVRE = LoadingMapOfDetachedVRE.get(detachedVREName);
			LOG.info("Detached VRE found {}", theDetachedVRE);
		}catch (ExecutionException e) {
			LOG.info("The Detached VRE for name {} not found",detachedVREName);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
