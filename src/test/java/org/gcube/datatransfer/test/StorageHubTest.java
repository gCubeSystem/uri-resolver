package org.gcube.datatransfer.test;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.StreamDescriptor;
import org.gcube.common.storagehub.client.plugins.AbstractPlugin;
import org.gcube.common.storagehub.client.proxies.ItemManagerClient;
import org.gcube.datatransfer.resolver.init.UriResolverSmartGearManagerInit;


/**
 * The Class CatalogueResolverTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * May 13, 2019
 */
public class StorageHubTest {

	private static String entityName = "using_e-infrastructures_for_biodiversity_conservation";
	private static String entityContext = "ctlg";
	private static String vreName = "BlueBridgeProject";
	
	private String rootContextScope = "/d4science.research-infrastructures.eu";
	private String authorizationToken = "";
	
	//@Before
	public void init() {

		UriResolverSmartGearManagerInit.setRootContextScope(rootContextScope);
	}

	//@Test
	public void testStreamDescriptorInfo() {
		System.out.println("testStreamDescriptorInfo starts...");
		ScopeProvider.instance.set(rootContextScope);
		SecurityTokenProvider.instance.set(authorizationToken);
		String storageHubId = "E_NGJIUEYvU09sNG1YY0R2VGIyaStWdGhDSW9sSjRNdDRkdVI2RHRGb1BZMVBaVFlzMG1mOU5QUEtFM1hQeE9kbw==";
		try{

			ItemManagerClient client = AbstractPlugin.item().build();
			StreamDescriptor descriptor = client.resolvePublicLink(storageHubId);
			System.out.println("Descriptor: "+descriptor);

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
}
