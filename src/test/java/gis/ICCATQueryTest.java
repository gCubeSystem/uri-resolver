//package gis;
//import it.geosolutions.geonetwork.util.GNSearchRequest;
//import it.geosolutions.geonetwork.util.GNSearchResponse;
//
//import java.io.BufferedInputStream;
//import java.io.BufferedReader;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.gcube.common.encryption.StringEncrypter;
//import org.gcube.common.scope.api.ScopeProvider;
//import org.gcube.spatial.data.geonetwork.GeoNetwork;
//import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
//import org.gcube.spatial.data.geonetwork.LoginLevel;
//import org.gcube.spatial.data.geonetwork.configuration.Configuration;
//import org.gcube.spatial.data.geonetwork.model.Account;
//import org.gcube.spatial.data.geonetwork.model.Account.Type;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.NodeList;
//
///**
// *
// * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
// * Aug 31, 2016
// */
//public class ICCATQueryTest {
//
//	private static final int MAX = 200;
//
//	//private String[] scopes = {"/gcube/devNext/NextNext"};
//
//	//private String[] scopesProd = {"/d4science.research-infrastructures.eu"};
//
//	private static String[] scopesProd = {"/d4science.research-infrastructures.eu/gCubeApps/ICCAT_BFT-E"};
//
//	//private String[] scopesProd = {"/d4science.research-infrastructures.eu/gCubeApps/fisheriesandecosystematmii"};
//
//	//private String[] scopesProd = {"/d4science.research-infrastructures.eu/D4Research"};
//
//	//private String[] scopesProd = {"/d4science.research-infrastructures.eu"};
//
//	private static LoginLevel loginLevel = LoginLevel.CKAN;
//
//	private static Type accountType = Type.SCOPE;
//
//	private static String textToSearch = "geo_fea";
//
//	private static Logger logger = LoggerFactory.getLogger(ICCATQueryTest.class);
//
//	private static enum LAYER_TYPE {PRIVATE, PUBLIC, ALL}
//
//
//	public static List<String> getLayers(LAYER_TYPE type) throws Exception{
//
//		try{
//			for(String scope:scopesProd){
//				ScopeProvider.instance.set(scope);
//				GeoNetworkPublisher reader=GeoNetwork.get();
//
//				Configuration config = reader.getConfiguration();
//				Account account=config.getScopeConfiguration().getAccounts().get(accountType);
//
//				//System.out.println("User: "+account.getUser()+", Pwd: "+account.getPassword());
//				System.out.println("Admin: "+config.getAdminAccount().getUser()+", Pwd: "+config.getAdminAccount().getPassword());
//
//				try{
//					String decryptedPassword = StringEncrypter.getEncrypter().decrypt(account.getPassword());
//					System.out.println("Decrypted Password: "+decryptedPassword);
//				}catch(Exception e){
//					System.out.println("ignoring exception during pwd decrypting");
//				}
//
//
//	//			req.addParam("keyword", "Thredds");
//				final GNSearchRequest req=new GNSearchRequest();
////				req.addParam(GNSearchRequest.Param.any,"Thredds");
//				GNSearchResponse resp = reader.query(req);
//				int publicCount=resp.getCount();
//
//				List<String> publicLayers = new ArrayList<String>();
//					for(int i=0; i<publicCount; i++)
//						publicLayers.add(resp.getMetadata(i).getUUID());
//
//				System.out.println("Public layers are: "+publicLayers.size());
//
//				//PERFOMING LOGIN TO GET ALL LAYERS
//				reader.login(loginLevel);
//				resp = reader.query(req);
//				int totalCount=resp.getCount();
//				int privateCount = totalCount-publicCount;
//
//				System.out.println("SCOPE "+scope+" found "+totalCount+" (public : "+publicCount+", private :"+privateCount+")");
//
//				List<String> privateLayers = new ArrayList<String>();
//				for(int i=0; i<totalCount; i++){
//
//					String uuid = resp.getMetadata(i).getUUID();
//					//System.out.println("Searching UUID: "+uuid);
//					if(!publicLayers.contains(uuid)){
//						privateLayers.add(uuid);
//					}
//
////						String xml = reader.getByIdAsRawString(resp.getMetadata(i).getUUID());
////						InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
////						iccatGNUUID.add(toGetFileIdentifier(stream));
//
//					//System.out.println(i+") is Thredds? "+containsString(xml, "Thredds"));
//				}
//
//				System.out.println("Private layers are: "+privateLayers.size());
//
//
//				switch (type) {
//				case PUBLIC:
//					return publicLayers;
//				case PRIVATE:
//					return privateLayers;
//				case ALL:
//					 publicLayers.addAll(privateLayers);
//					 return publicLayers;
//				default:
//					break;
//				}
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	/**
//	 * Override summary record.
//	 *
//	 * @param doc the doc
//	 * @param identifier the identifier
//	 * @return true, if successful
//	 */
//	private static String getFileIdentifierValue(Document doc) {
//
//		// <csw:SummaryRecord> list
//		NodeList nodes = doc.getElementsByTagName("gmd:MD_Metadata");
//		logger.debug("gmd:MD_Metadata are: " + nodes.getLength());
//		for (int i = 0; i < nodes.getLength(); i++) {
//			Element mdMetadata = (Element) nodes.item(i);
//
//			// <dc:identifier>
//			NodeList fileIdentifierLst = mdMetadata.getElementsByTagName("gmd:fileIdentifier");
//			if(fileIdentifierLst==null || fileIdentifierLst.getLength()==0 || fileIdentifierLst.item(0)==null){
//				logger.info("skipping, it has not fileidentifier");
//				return null;
//			}
//
//			Element id = (Element) fileIdentifierLst.item(0);
//
//			NodeList gcoLst =  id.getElementsByTagName("gco:CharacterString");
//			if(gcoLst==null || gcoLst.getLength()==0 || gcoLst.item(0)==null){
//				logger.info("skipping, it has not gco:CharacterString");
//				return null;
//			}
//
//			Element gco = (Element) gcoLst.item(0);
//			String idValue = gco.getTextContent();
//			logger.trace("Summary gmd:fileIdentifier is: " + idValue);
//			return idValue;
//		}
//
//		return null;
//	}
//
//	/**
//	 * Removes the summary ids by list ids.
//	 *
//	 * @param getRecordsResponse the get records response
//	 * @param idsToRemove the ids to remove
//	 * @return the input stream
//	 * @throws IOException Signals that an I/O exception has occurred.
//	 */
//	public static String toGetFileIdentifier(InputStream getRecordResponse) throws IOException {
//
//		try {
//			// logger.trace("getRecordsResponse is: "+IOUtils.toString(getRecordsResponse));
//			BufferedInputStream bis = new BufferedInputStream(getRecordResponse);
//			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//			dbf.setValidating(false);
//			DocumentBuilder db = dbf.newDocumentBuilder();
//			Document doc = db.parse(bis);
//			return getFileIdentifierValue(doc);
//		}
//		catch (Exception e) {
//			logger.error("An error occurred during removing IDS by List: ", e);
//			return null;
//		}
//	}
//
//
//	public static List<String> fileReader() throws FileNotFoundException{
//
//		String fileName = "iccat_name_catalogue.csv";
//
////		//read file into stream, try-with-resources
////		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
////
////			stream.forEach(System.out::println);
////
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
//
//		FileReader reportReader = new FileReader(fileName);
//		//Stream<String> stream = reportBW.lines();
//		List<String> list = new ArrayList<>();
//		try (BufferedReader reportBW = new BufferedReader(reportReader)){
//
//			list = reportBW.lines().collect(Collectors.toList());
//
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return list;
//	}
//
//
//	public static void main(String[] args) {
//
//		List<String> listCKAN;
//		try {
//			listCKAN = fileReader();
//			System.out.println("CKAN Layers are: "+listCKAN.size());
////			for (String line : listCKAN) {
////				System.out.println("CKAN UUID: "+line);
////			}
//
//			List<String> listLayersUUID = getLayers(LAYER_TYPE.PUBLIC);
//			System.out.println("List Layers UUID are: "+listLayersUUID.size());
////			for (String line : listLayersUUID) {
////				System.out.println("PRIVATE GN UUID: "+line);
////			}
//
//
//			listCKAN.removeAll(listLayersUUID);
//
//			System.out.println("\n\n\nCKAN contains other: "+listCKAN.size());
//			for (String line : listCKAN) {
//				System.out.println("CKAN UUID: "+line);
//			}
//
//		}catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//
//
//	}
//}
