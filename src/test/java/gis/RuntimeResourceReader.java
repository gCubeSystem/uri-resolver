package gis;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class RuntimeResourceReader.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Mar 10, 2017
 */
public class RuntimeResourceReader {

	// public static final String GEONETWORK_END_POINT =
	// "http://geonetwork.d4science.org/geonetwork";

	public static final Logger logger = LoggerFactory.getLogger(RuntimeResourceReader.class);

	// private List<String> scopes = new ArrayList<String>();

	private List<ServiceEndpointBean> listSE = new ArrayList<ServiceEndpointBean>();

	/**
	 * Instantiates a new runtime resource reader.
	 *
	 * @param scope        the scope
	 * @param platformName the platform name
	 * @param endPoint     the end point
	 * @throws Exception the exception
	 */
	public RuntimeResourceReader(String scope, String platformName, String category, String endPoint) throws Exception {
		read(scope, platformName, category, endPoint);
	}

	/**
	 * Read.
	 *
	 * @param scope        the scope
	 * @param platformName the platform name
	 * @param endPoint     the end point
	 * @return the server parameters
	 * @throws Exception the exception
	 */
	private List<ServiceEndpointBean> read(String scope, String platformName, String category, String endPoint)
			throws Exception {

		String originalScope = null;
		try {

			originalScope = ScopeProvider.instance.get();

			ScopeProvider.instance.set(scope);

			SimpleQuery query = queryFor(ServiceEndpoint.class);
			if (platformName != null)
				query.addCondition("$resource/Profile/Platform/Name/text() eq '" + platformName + "'");
			if (category != null)
				query.addCondition("$resource/Profile/Category/text() eq '" + category + "'");
			if (endPoint != null && !endPoint.isEmpty())
				query.addCondition("$resource/Profile/AccessPoint/Interface/Endpoint/text() eq '" + endPoint + "'");

//			query.addVariable("$prop", "$resource/Profile/AccessPoint/Properties/Property")
//			.addCondition("$prop/Name/text() eq 'priority'")
//			.addCondition("$prop/Value/text() eq '1'");

			logger.info("GeoRuntimeReader, using scope: " + scope + ", to get resource: " + platformName);

			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);

			List<ServiceEndpoint> listServiceEndpoint = client.submit(query);
			if (listServiceEndpoint == null || listServiceEndpoint.isEmpty())
				throw new Exception("Cannot retrieve the runtime resource: " + platformName);

			for (ServiceEndpoint serviceEndpoint : listServiceEndpoint) {

				ServiceEndpointBean seb = new ServiceEndpointBean();

				seb.setRuntime(serviceEndpoint.profile().runtime());

				List<AccessPoint> listAp = new ArrayList<ServiceEndpoint.AccessPoint>();
				try {
					for (AccessPoint accessPoint : serviceEndpoint.profile().accessPoints()) {
						listAp.add(accessPoint);
					}
				} catch (Exception e) {
					System.err.println("Error on reading Access point not found");
				}

				seb.setListAP(listAp);
				listSE.add(seb);
			}

		} catch (Exception e) {
			logger.error("Sorry, an error occurred on reading parameters in Runtime Resources", e);
		} finally {
			if (originalScope != null && !originalScope.isEmpty()) {
				ScopeProvider.instance.set(originalScope);
				logger.info("scope provider setted to orginal scope: " + originalScope);
			} else {
				ScopeProvider.instance.reset();
				logger.info("scope provider reset");
			}
		}

		return listSE;
	}

	public List<ServiceEndpointBean> getListSE() {
		return listSE;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

//		String scope = "/pred4s/preprod/preVRE";
//		String scope = "/d4science.research-infrastructures.eu/D4OS/GNA";
		 String scope = "/gcube/devsec/devVRE";
//		String scope = "/d4science.research-infrastructures.eu/gCubeApps/ProtectedAreaImpactMaps";
		// String scope = "/d4science.research-infrastructures.eu/gCubeApps/Esquiline";
//		String scope = "/d4science.research-infrastructures.eu/D4OS/ARIADNEplus_Project";

//		String platformName = "geonetwork";
//		String category = "Gis";

		String platformName = "https://api.mapbox.com";
		//String category = null;
		String category = "OnlineService";

//		String platformName = "postgis";
//		String category = "Database";

//		String platformName = "postgres";
//		String category = "Database";

//		String platformName = "mongodb";
//		String category = "Database";

		// scope = "/pred4s/preprod/preVRE";

//		scope = "/d4science.research-infrastructures.eu/D4OS/Blue-Cloud2026Project";
//		platformName = "Zenodo";
//		category = "Repository";

		RuntimeResourceReader reader;
		try {
			ScopeProvider.instance.set(scope);
			reader = new RuntimeResourceReader(scope, platformName, category, null);

			for (ServiceEndpointBean seb : reader.getListSE()) {
				System.out.println("Found: " + seb);

				List<AccessPoint> listAp = seb.getListAP();

				for (AccessPoint ap : listAp) {
					System.out.println("username: " + ap.username());
					System.out.println("password: " + ap.password());
					try {
						String decryptedPassword = StringEncrypter.getEncrypter().decrypt(ap.password());
						System.out.println("Decrypted Password: " + decryptedPassword);
					} catch (Exception e) {
						System.out.println("ignoring exception during pwd decrypting");
					}

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
