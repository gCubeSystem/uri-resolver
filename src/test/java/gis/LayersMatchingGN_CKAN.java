/**
 *
 */
package gis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.spatial.data.geonetwork.GeoNetwork;
import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
import org.gcube.spatial.data.geonetwork.LoginLevel;

import it.geosolutions.geonetwork.util.GNSearchRequest;
import it.geosolutions.geonetwork.util.GNSearchResponse;
import resources.GetAllInfrastructureScopes;


/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 10, 2017
 */
public class LayersMatchingGN_CKAN {

	//GN CONFIGURATIONS
	public static String rootScope = "/d4science.research-infrastructures.eu";
	//static String rootScope = "/pred4s";
//	static String rootScope = "/gcube";
	public static String platformName = "geonetwork";
	public static LoginLevel loginLevel = LoginLevel.ADMIN;
	//static Type accountType = Type.SCOPE;
	public static String textToSearch = "geo_fea";

	public static PrintWriter reportPrintWriter;
	public static PrintWriter errorPrintWriter;
	
	public static void main(String[] args) {
		
		try{
			FileWriter reportWriter = new FileWriter(rootScope.substring(1,rootScope.length())+"_report_matching_gn_catalogue.csv", true);
			FileWriter errorWriter = new FileWriter(rootScope.substring(1,rootScope.length())+"_error_matching_gn_catalogue.csv", true);
			BufferedWriter reportBW = new BufferedWriter(reportWriter);
			BufferedWriter errorBW = new BufferedWriter(errorWriter);
			reportPrintWriter = new PrintWriter(reportBW);
			reportPrintWriter.println("NB.; SCOPE; GN Endpoint; TOTAL LAYERS; PUBLIC LAYERS; PRIVATE LAYERS");
			errorPrintWriter = new PrintWriter(errorBW);
			errorPrintWriter.println("SCOPE;");
		}catch(Exception e){
			e.printStackTrace();
		}

//		final Path destination = Paths.get("report_matching_gn_catalogue.csv");
//		Files.w(re, destination);

		List<String> scopesProd = new ArrayList<String>();

		//RuntimeResourceReader readerRR;
		try {
			//MODE-1
//			readerRR = new RuntimeResourceReader(rootScope, platformName, null);
//			System.out.println(readerRR.toString());
//			System.out.println("Using GN: "+readerRR.getParameters());
//			System.out.println("Scopes are: "+readerRR.getScopes().size());
//			scopesProd.addAll(readerRR.getScopes());
			
			//MODE-2
			Map<String,String> mapScopes = GetAllInfrastructureScopes.loadMapOfScopeNameToFullScope(rootScope);
			scopesProd.addAll(mapScopes.values());
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Read "+scopesProd.size() +" VREs from root scope: "+rootScope);
		int i = 0;
		for (String vre : scopesProd) {
			System.out.println(++i +"; "+vre);
		}
		
		System.out.println("Scope found: "+scopesProd.size());

		int count = 0;
		try{

			for(String scope:scopesProd){
				count++;
				System.out.println("\n\n#"+count+" of "+scopesProd.size()+ " - Fetching scope: "+scope);
				ScopeProvider.instance.set(scope);
				GeoNetworkPublisher reader = null;
				String gnEP = null;
				try{
					reader=GeoNetwork.get();
					gnEP = reader.getConfiguration().getGeoNetworkEndpoint();
					
				}catch (Exception e) {
					System.out.println("GeoNetwork not instanciable in the scope: "+scope);
					writeError(count+"; "+scope+" ; "+e.getMessage());
				}

				if(reader!=null){
//					Configuration config = reader.getConfiguration();
//					Account account=config.getScopeConfiguration().getAccounts().get(accountType);
//
//					//System.out.println("User: "+account.getUser()+", Pwd: "+account.getPassword());
//					System.out.println("Admin: "+config.getAdminAccount().getUser()+", Pwd: "+config.getAdminAccount().getPassword());
//	
//					try{
//						String decryptedPassword = StringEncrypter.getEncrypter().decrypt(account.getPassword());
//						System.out.println("Decrypted Password: "+decryptedPassword);
//					}catch(Exception e){
//						System.out.println("ignoring exception during pwd decrypting");
//					}


		//			req.addParam("keyword", "Thredds");
					final GNSearchRequest req=new GNSearchRequest();
	//				req.addParam(GNSearchRequest.Param.any,"Thredds");
					GNSearchResponse resp = reader.query(req);
					int publicCount=resp.getCount();
					reader.login(loginLevel);
					int totalCount=reader.query(req).getCount();
					//System.out.println("SCOPE "+scope+" ; found "+totalCount+" (public : "+publicCount+", private :"+(totalCount-publicCount)+")");
					writeReport(count+"; "+scope+" ; "+gnEP+"; "+totalCount+" ; "+publicCount+"; "+(totalCount-publicCount));
					if(totalCount==0)
						return;
//					try{
//						int last = totalCount>MAX?totalCount:MAX;
//						for(int i=0; i<last; i++){
//							String xml = reader.getByIdAsRawString(resp.getMetadata(i).getUUID());
//							System.out.println(i+") is Thredds? "+containsString(xml, "Thredds"));
//						}
//					}catch(Exception e ){
//						e.printStackTrace();
//					}
				}

			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(reportPrintWriter!=null)
				reportPrintWriter.close();

			if(errorPrintWriter!=null)
				errorPrintWriter.close();

			System.out.println("Performed fetching from "+count+" scopes");
			System.out.println("FINISHED!!!");
		}
	}

	private static void writeReport(String newline){

		reportPrintWriter.println(newline);
	}

	private static void writeError(String newline){

		errorPrintWriter.println(newline);
	}

}
