package gis;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.spatial.data.geonetwork.GeoNetwork;
import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
import org.gcube.spatial.data.geonetwork.GeoNetworkReader;
import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.gcube.spatial.data.geonetwork.configuration.Configuration;
import org.gcube.spatial.data.geonetwork.model.Account;
import org.gcube.spatial.data.geonetwork.model.Account.Type;

import it.geosolutions.geonetwork.util.GNSearchRequest;
import it.geosolutions.geonetwork.util.GNSearchResponse;
import it.geosolutions.geonetwork.util.GNSearchResponse.GNMetadata;

/**
 * The Class GeonetworkQueryTest.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Aug 31, 2016
 */
public class GeonetworkQueryTest {

	private static final int MAX = 30;

	//private String[] scopes = {"/gcube/devsec/devVRE"};
	
	//private String[] scopes = {"/gcube/devsec/StaTabTest"};
	
	//private String[] scopes = {"/gcube/devsec/devVRE"};

	//private String[] scopes = {"/pred4s/preprod/preVRE"};
	
	//private String[] scopes = {"/d4science.research-infrastructures.eu"};

	//private String[] scopesProd = {"/d4science.research-infrastructures.eu/gCubeApps/fisheriesandecosystematmii"};

	//private String[] scopesProd = {"/d4science.research-infrastructures.eu/D4Research"};

	//private String[] scopesProd = {"/d4science.research-infrastructures.eu"};
	
	//private String[] scopes = {"/d4science.research-infrastructures.eu/gCubeApps/BlueBridgeProject"};
	
	//private String[] scopes = {"/d4science.research-infrastructures.eu/gCubeApps/AquacultureAtlasGeneration"};
	
	//private String[] scopes = {"/d4science.research-infrastructures.eu/gCubeApps/FrenchTropicalTunaAtlas"};
	
	private String[] scopes = {"/d4science.research-infrastructures.eu/gCubeApps/BiodiversityLab"};
	
	//private String[] scopes = {"/d4science.research-infrastructures.eu/gCubeApps/SIASPA"};
	
	//private String[] scopes = {"/d4science.research-infrastructures.eu/gCubeApps/ICCAT_BFT-E"};
	 
	

	private LoginLevel loginLevel = LoginLevel.CKAN;

	private Type accountType = Type.CKAN;

	private String textToSearch = "geo_fea";

	/**
	 * Gets the count.
	 *
	 * @return the count
	 * @throws Exception the exception
	 */
	//@Test
	public void getCount() throws Exception{

		try{
			for(String scope:scopes){
				ScopeProvider.instance.set(scope);
				GeoNetworkPublisher reader=GeoNetwork.get();

				Configuration config = reader.getConfiguration();
				Account account=config.getScopeConfiguration().getAccounts().get(accountType);
				
//				Configuration config = gnInstance.getGeonetworkPublisher().getConfiguration();
//				Account account = config.getScopeConfiguration().getAccounts().get(Type.CKAN);

				//System.out.println("User: "+account.getUser()+", Pwd: "+account.getPassword());
				System.out.println("Admin: "+config.getAdminAccount().getUser()+", Pwd: "+config.getAdminAccount().getPassword());
				System.out.println("USER: "+account.getUser()+", Pwd: "+account.getPassword());
				System.out.println("GN: "+config.getGeoNetworkEndpoint());
				try{
					String decryptedPassword = StringEncrypter.getEncrypter().decrypt(account.getPassword());
					System.out.println("Decrypted Password: "+decryptedPassword);
				}catch(Exception e){
					System.out.println("ignoring exception during pwd decrypting");
				}


	//			req.addParam("keyword", "Thredds");
				final GNSearchRequest req=new GNSearchRequest();
//				req.addParam(GNSearchRequest.Param.any,"Thredds");
				GNSearchResponse resp = reader.query(req);
				int publicCount=resp.getCount();
				System.out.println("Public UUIDs are: "+publicCount);
				Map<Long,String> publicUUIDs = getUUIDs(resp);
				System.out.println("Filled list of Public UUIDs with: "+publicUUIDs.size());
				System.out.println("Public layers are: ");
				printLayers(publicUUIDs);
				
				reader.login(loginLevel);
				resp = reader.query(req);
				int totalCount=resp.getCount();
				System.out.println("All UUIDs are: "+totalCount);
				Map<Long,String> allUUIDs = getUUIDs(resp);
				System.out.println("Filled list of All UUIDs with: "+allUUIDs.size());
				
				System.out.println("SCOPE "+scope+" found "+totalCount+" (public : "+publicCount+", private :"+(totalCount-publicCount)+")");
				
				System.out.println("Private layers are: ");
				allUUIDs.keySet().removeAll(publicUUIDs.keySet());
				printLayers(allUUIDs);
				
				/*if(totalCount==0)
					return;
				try{
					int last = totalCount<MAX?totalCount:MAX;
					for(int i=0; i<last; i++){
						String xml = reader.getByIdAsRawString(resp.getMetadata(i).getUUID());
						System.out.println(i+") UUID: "+resp.getMetadata(i).getUUID() + " ID: "+resp.getMetadata(i).getId());
						//System.out.println(i+") is Thredds? "+containsString(xml, "Thredds"));
					}
					
				}catch(Exception e ){
					e.printStackTrace();
				}*/
				System.out.println("DONE!!!");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Prints the layers.
	 *
	 * @param layersUUIDs the layers UUI ds
	 */
	public void printLayers(Map<Long,String> layersUUIDs) {
		int index = 0;

		for (Long key : layersUUIDs.keySet()) {
			index++;
			System.out.println(index+") ID: "+key+" - UUID: "+layersUUIDs.get(key));
		}
	}
	

	/**
	 * Gets the UUI ds.
	 *
	 * @param resp the resp
	 * @return the UUI ds
	 */
	public static Map<Long,String> getUUIDs(GNSearchResponse resp){
		
		Map<Long,String> listUUIDs = new HashMap<Long,String>();
		Iterator<GNMetadata> iterator = resp.iterator();
		while(iterator.hasNext()) {
			GNMetadata meta = iterator.next();
			//listUUIDs.add(new GN_META_REF(meta.getId(), meta.getUUID()));
			listUUIDs.put(meta.getId(), meta.getUUID());
			//System.out.println("Added: "+meta.getUUID());
		}
		
		return listUUIDs;
	}


	/**
	 * Gets the layers by search.
	 *
	 * @return the layers by search
	 * @throws Exception the exception
	 */
	//@Test
	public void getLayersBySearch() throws Exception{
		try{
			for(String scope:scopes){
				ScopeProvider.instance.set(scope);
				GeoNetworkPublisher reader=GeoNetwork.get();



				Configuration config = reader.getConfiguration();
				Account account=config.getScopeConfiguration().getAccounts().get(accountType);

				//System.out.println("User: "+account.getUser()+", Pwd: "+account.getPassword());
				System.out.println("Admin: "+config.getAdminAccount().getUser()+", Pwd: "+config.getAdminAccount().getPassword());

				try{
					String decryptedPassword = StringEncrypter.getEncrypter().decrypt(account.getPassword());
					System.out.println("Decrypted Password: "+decryptedPassword);
				}catch(Exception e){
					System.out.println("ignoring exception during pwd decrypting");
				}


	//			req.addParam("keyword", "Thredds");
				//final GNSearchRequest req=new GNSearchRequest();
				final GNSearchRequest req = getRequest(true, textToSearch);
//				req.addParam(GNSearchRequest.Param.any,"Thredds");
				GNSearchResponse resp = reader.query(req);
				int publicCount=resp.getCount();
				reader.login(loginLevel);
				int totalCount=reader.query(req).getCount();
				System.out.println("QUERY "+textToSearch);
				System.out.println("SCOPE "+scope+" found "+totalCount+" (public : "+publicCount+", private :"+(totalCount-publicCount)+")");

//				if(totalCount==0)
//					return;
//				try{
//					int last = totalCount>MAX?totalCount:MAX;
//
//					for(int i=0; i<last; i++){
//						//String xml = reader.getByIdAsRawString(resp.getMetadata(i).getUUID());
//						GNMetadata xml = resp.getMetadata(i);
//						//System.out.println(i+") is Thredds? "+containsString(xml, "Thredds"));
//						System.out.println(xml.toString());
//					}
//				}catch(Exception e ){
//					e.printStackTrace();
//				}

			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	/**
	 * Contains string.
	 *
	 * @param txt the txt
	 * @param value the value
	 * @return true, if successful
	 */
	private boolean containsString(String txt, String value){
		return txt.contains(value);
	}

	/**
	 * Gets the request.
	 *
	 * @param sortByTitle the sort by title
	 * @param textToSearch the text to search
	 * @return the request
	 */
	public GNSearchRequest getRequest(boolean sortByTitle, String textToSearch) {

		GNSearchRequest req = new GNSearchRequest();

		if(sortByTitle)
			req.addConfig(GNSearchRequest.Config.sortBy, "title");

		if(textToSearch==null || textToSearch.isEmpty()){
			req.addParam(GNSearchRequest.Param.any, textToSearch);
			System.out.println("search by any text");
		}else{
			req.addParam(GNSearchRequest.Param.title, textToSearch);
			req.addConfig(GNSearchRequest.Config.similarity, "1");
			System.out.println("search by title");
		}
		System.out.println("text to search "+textToSearch);

		return req;
	}
	
	/**
	 * The Class GN_META_REF.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * May 20, 2019
	 */
	public static class GN_META_REF{
		Long id;
		String uuid;
		
		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public Long getId() {
			return id;
		}
		
		/**
		 * Sets the id.
		 *
		 * @param id the new id
		 */
		public void setId(Long id) {
			this.id = id;
		}
		
		/**
		 * Gets the uuid.
		 *
		 * @return the uuid
		 */
		public String getUuid() {
			return uuid;
		}
		
		/**
		 * Sets the uuid.
		 *
		 * @param uuid the new uuid
		 */
		public void setUuid(String uuid) {
			this.uuid = uuid;
		}
		
		/**
		 * Instantiates a new gn meta ref.
		 *
		 * @param id the id
		 * @param uuid the uuid
		 */
		public GN_META_REF(Long id, String uuid) {
			super();
			this.id = id;
			this.uuid = uuid;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("GN_META_REF [id=");
			builder.append(id);
			builder.append(", uuid=");
			builder.append(uuid);
			builder.append("]");
			return builder.toString();
		}

	}
	


/**
 * Gets the count prod.
 *
 * @return the count prod
 * @throws Exception the exception
 */
//	@Test
	public void getCountProd() throws Exception{
		try{
			for(String scope:scopes){
				ScopeProvider.instance.set(scope);
				GeoNetworkReader reader=GeoNetwork.get();
				final GNSearchRequest req=new GNSearchRequest();
	//			req.addParam("keyword", "Thredds");
//				req.addParam(GNSearchRequest.Param.any,"Oscar");
				int publicCount=reader.query(req).getCount();
				reader.login(loginLevel);
				int totalCount=reader.query(req).getCount();
				System.out.println("SCOPE "+scope+" found "+totalCount+" (public : "+publicCount+", private :"+(totalCount-publicCount)+")");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
