/**
 *
 */
package gis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.spatial.data.geonetwork.LoginLevel;

import resources.GetAllInfrastructureScopes;

/**
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Mar 10, 2017
 */
public class SE_Harvester_from_IS {

	// GN CONFIGURATIONS
//	public static String rootScope = "/d4science.research-infrastructures.eu";
	static String rootScope = "/pred4s/preprod/preVRE";
//	static String rootScope = "/gcube";
	public static final String platformName = "geoserver";
	public static final String category = "Gis";
	public static LoginLevel loginLevel = LoginLevel.ADMIN;

	public static PrintWriter reportPrintWriter;
	public static PrintWriter errorPrintWriter;

	public static HashSet<String> uniqueGNs = new HashSet<String>();

	public static void main(String[] args) {

		try {
			FileWriter reportWriter = new FileWriter(
					rootScope.substring(1, rootScope.length()) + "_report_" + platformName + "_harvester.csv", true);
			FileWriter errorWriter = new FileWriter(
					rootScope.substring(1, rootScope.length()) + "_error_" + platformName + "_harvester.csv", true);
			BufferedWriter reportBW = new BufferedWriter(reportWriter);
			BufferedWriter errorBW = new BufferedWriter(errorWriter);
			reportPrintWriter = new PrintWriter(reportBW);
			reportPrintWriter.println("NB.; SCOPE; SE Hosted_On; SE AccessPoint;");
			errorPrintWriter = new PrintWriter(errorBW);
			errorPrintWriter.println("SCOPE;");
		} catch (Exception e) {
			e.printStackTrace();
		}

//		final Path destination = Paths.get("report_matching_gn_catalogue.csv");
//		Files.w(re, destination);

		List<String> listScopes = new ArrayList<String>();

		// RuntimeResourceReader readerRR;
		try {
			// MODE-1
//			readerRR = new RuntimeResourceReader(rootScope, platformName, null);
//			System.out.println(readerRR.toString());
//			System.out.println("Using GN: "+readerRR.getParameters());
//			System.out.println("Scopes are: "+readerRR.getScopes().size());
//			scopesProd.addAll(readerRR.getScopes());

			// MODE-2
			Map<String, String> mapScopes = GetAllInfrastructureScopes.loadMapOfScopeNameToFullScope(rootScope);
			listScopes.addAll(mapScopes.values());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Read " + listScopes.size() + " VREs from root scope: " + rootScope);
		int i = 0;
		for (String vre : listScopes) {
			System.out.println(++i + "; " + vre);
		}

		System.out.println("Scope found: " + listScopes.size());
		try {
			
			searchFor(listScopes, platformName, category);
			searchFor(listScopes, "GeoServer", category);

			System.out.println("\n\nUnique " + platformName + " addresses");
			uniqueGNs.stream().forEach(s -> System.out.println(s));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reportPrintWriter != null)
				reportPrintWriter.close();

			if (errorPrintWriter != null)
				errorPrintWriter.close();

			System.out.println("Performed fetching from " + listScopes.size() + " scopes");
			System.out.println("FINISHED!!!");
		}
	}

	private static void searchFor(List<String> listScopes, String platformName, String category) {

		int count = 0;
		int writerCount = 0;
		try {

			for (String scope : listScopes) {
				count++;
				System.out.println("\n\n#" + count + " of " + listScopes.size() + " - Fetching scope: " + scope);

				RuntimeResourceReader rrr = new RuntimeResourceReader(scope, platformName, category, null);

				List<ServiceEndpointBean> listSE = rrr.getListSE();

				for (ServiceEndpointBean seb : listSE) {

					StringBuilder sb = new StringBuilder();
					for (AccessPoint ap : seb.getListAP()) {
						sb.append(ap.address());
						sb.append(", ");

						uniqueGNs.add(ap.address());
					}

					sb.toString();
					writerCount++;
					writeReport(
							writerCount + "; " + scope + " ; " + seb.getRuntime().hostedOn() + "; " + sb.toString());

				}

				Thread.sleep(200);

//				if(count==10)
//					break;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void writeReport(String newline) {

		reportPrintWriter.println(newline);
	}

	private static void writeError(String newline) {

		errorPrintWriter.println(newline);
	}

}
