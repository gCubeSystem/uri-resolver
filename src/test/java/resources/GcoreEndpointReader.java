package resources;
import static org.gcube.resources.discovery.icclient.ICFactory.client;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class GcoreEndpointReader.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Jun 10, 2016
 */
public class GcoreEndpointReader implements Serializable{

	private static final long serialVersionUID = 7631710353375893823L;

	private static final Logger logger = LoggerFactory.getLogger(GcoreEndpointReader.class);
	private List<String> endPoints;

	/**
	 * Instantiates a new gcore endpoint reader.
	 *
	 * @param scope the scope
	 * @throws Exception the exception
	 */
	public GcoreEndpointReader(String scope, String serviceClass, String serviceName, String ckanResource) throws Exception {

		String currentScope = ScopeProvider.instance.get();
		try{

			logger.info("set scope "+scope);
			ScopeProvider.instance.set(scope);

			SimpleQuery query = queryFor(GCoreEndpoint.class);
			query.addCondition(String.format("$resource/Profile/ServiceClass/text() eq '%s'",serviceClass));
			query.addCondition("$resource/Profile/DeploymentData/Status/text() eq 'ready'");
			query.addCondition(String.format("$resource/Profile/ServiceName/text() eq '%s'",serviceName));
			query.addCondition(String.format("$resource/Scopes/Scope/text()[.='%s']", scope)); // i.e. check the resource contains among the scopes this one
			query.setResult("$resource/Profile/AccessPoint/RunningInstanceInterfaces//Endpoint[@EntryName/string() eq \""+ckanResource+"\"]/text()");

			logger.debug("submitting quey "+query.toString());

			DiscoveryClient<String> client = client();
			List<String> endpoints = client.submit(query);
			if (endpoints == null || endpoints.isEmpty()) throw new Exception("Cannot retrieve the GCoreEndpoint serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+scope);


			for (String endpoint : endpoints) {
				System.out.println("end point "+endpoint);
			}
			this.endPoints = endpoints;
			if(endPoints==null)
				throw new Exception("Endpoint:"+ckanResource+", is null for serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+scope);

			logger.info("found entyname "+endPoints+" for ckanResource: "+ckanResource);

			/*Group<Endpoint> accessPoints = se.profile().endpoints();
			if(accessPoints.size()==0) throw new Exception("Endpoint in serviceName serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+scope +" not found");

			Endpoint ep = accessPoints.iterator().next();

			String epName = ep.name();

			System.out.println(epName);*/

		}catch(Exception e){
			String error = "An error occurred during GCoreEndpoint discovery, serviceName: "+serviceName +", serviceClass: " +serviceClass +", in scope: "+scope +".";
			logger.error(error, e);
			throw new Exception(error);
		}finally{
			logger.info("scope provider reset");
			ScopeProvider.instance.set(currentScope);
		}
	}


	/**
	 * Gets the end points.
	 *
	 * @return the end points
	 */
	public List<String> getEndPoints() {

		return endPoints;
	}

//	private static String[] scopes = {"" +
//		"/d4science.research-infrastructures.eu/gCubeApps",
//		"/d4science.research-infrastructures.eu/FARM",
//		"/d4science.research-infrastructures.eu/D4Research",
//		"/d4science.research-infrastructures.eu/OpenAIRE",
//		"/d4science.research-infrastructures.eu/Edison",
//		"/d4science.research-infrastructures.eu/SmartArea",
//		"/d4science.research-infrastructures.eu/SoBigData"};
	static PrintWriter reportPrintWriter;
	static PrintWriter errorPrintWriter;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		FileWriter reportWriter = null;
		
		final String ckanResource = "org.gcube.data.access.ckanconnector.CkanConnector";
		final String serviceName = "CkanConnector";
		final String serviceClass = "DataAccess";
		
		final String rootScope = "/d4science.research-infrastructures.eu";
		
		try{
			reportWriter = new FileWriter("report_ckanconnector.csv", true);
			BufferedWriter reportBW = new BufferedWriter(reportWriter);
			reportPrintWriter = new PrintWriter(reportBW);
			reportPrintWriter.println("NB.; SCOPE; END POINTS;");

			try {
				int i = 0;
				Map<String, String> scopes = GetAllInfrastructureScopes.loadMapOfScopeNameToFullScope(rootScope);
				for (String scope : scopes.values()) {
					System.out.println("Ckecking scope: "+scope);
					ScopeProvider.instance.set(scope);
					try{
						i++;
						GcoreEndpointReader reader = new GcoreEndpointReader(scope, serviceClass, serviceName, ckanResource);
						System.out.println("Appending endpoints: "+reader.getEndPoints());
						reportPrintWriter.println(i+"; "+scope+"; "+reader.getEndPoints());
					}catch(Exception e){
						i--;
						e.printStackTrace();
					}
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(reportPrintWriter!=null)
				reportPrintWriter.close();
		}
	}
}
