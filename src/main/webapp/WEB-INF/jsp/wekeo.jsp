<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">

<script type="text/javascript">
	function goToURL(relativePath) {
		var x = "/wekeo/" + relativePath;
		window.open(x, "_blank");
	}
</script>

<style type="text/css">
body {
	background-color: #f8f6f2;
	color: #225f97;
	font-family: 'Droid Sans', arial, sans-serif;
	font-size: 16px;
}

.mydiv {
	top: 50%;
	left: 50%;
	width: 600px;
	height: 300px;
	margin-top: -150px; /*set to a negative number 1/2 of your height*/
	margin-left: -300px; /*set to a negative number 1/2 of your width*/
	/* 	border: 1px solid #ccc;  */
	/* 	background-color: #9b9b9b; */
	position: fixed;
	text-align: center;
	/* 	vertical-align: middle; */
}

.myTitle {
	font-size: 22px;
	font-weight: bold;
}

.myListOperations {
	margin-top: 20px;
	margin-bottom: 40px;
	color: #333;
}

.uri-footer {
	padding: 20px 20px;
	font-size: 14px;
}
</style>
</head>

<body>
	<div class="mydiv">
		<img alt=""
			src="https://www.d4science.org/image/layout_set_logo?img_id=12630" />
		<div class="myTitle">The Wekeo Resolver</div>
		<div class="myListOperations">
			Available Operations:
			<p>
				# authentication operator
				<button onclick="goToURL('gettoken')">Get Token</button>
				(gCube AuthN required)
			</p>
		</div>
		<div class="uri-footer">
			See wiki page at <a
				href="https://gcube.wiki.gcube-system.org/gcube/URI_Resolver#Wekeo_Resolver"
				target="_blank">gCube Wiki Wekeo Resolver</a>
		</div>
	</div>
</body>
</html>