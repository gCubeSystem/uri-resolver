package org.gcube.datatransfer.resolver.geoportal;

import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.applicationprofile.ApplicationProfileNotFoundException;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * The Class GeoportalConfigApplicationProfileReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 3, 2024
 */
public class GeoportalConfigApplicationProfileReader {

	private static Logger LOG = LoggerFactory.getLogger(GeoportalConfigApplicationProfileReader.class);
	private static final String RESOURCE_PROFILE_BODY = "/Resource/Profile/Body";
	public static final String SECONDARY_TYPE = "ApplicationProfile";

	private String secondaryType;
	private String scope;
	private String appID;
	private GeoportalConfigApplicationProfile geoportalConfigAppProfile;

	/**
	 * Instantiates a new geoportal config application profile reader.
	 *
	 * @param appID the app ID
	 * @throws Exception the exception
	 */
	public GeoportalConfigApplicationProfileReader(String appID) throws Exception {

		this.appID = appID;
		this.secondaryType = SECONDARY_TYPE;
		this.scope = ScopeProvider.instance.get();
		this.geoportalConfigAppProfile = readProfileFromInfrastructure();
	}

	/**
	 * Read profile from infrastrucure.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	private GeoportalConfigApplicationProfile readProfileFromInfrastructure() throws Exception {

		String queryString = getGcubeGenericQueryString(secondaryType, appID);
		LOG.info("Scope " + scope + ", trying to perform query: " + queryString);

		try {

			if (scope == null)
				throw new Exception("Scope is null, set scope into ScopeProvider");

			GeoportalConfigApplicationProfile profile = new GeoportalConfigApplicationProfile();

			LOG.info("Trying to fetch ApplicationProfile in the scope: " + scope + ", SecondaryType: " + secondaryType
					+ ", AppId: " + appID);
			Query q = new QueryBox(queryString);
			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);
//			String item_fields = "";

			if (appProfile == null || appProfile.size() == 0)
				throw new ApplicationProfileNotFoundException("ApplicationProfile with SecondaryType: " + secondaryType
						+ ", AppId: " + appID + " is not registered in the scope: " + scope);
			else {
				String elem = appProfile.get(0);
				DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document doc = docBuilder.parse(new InputSource(new StringReader(elem)));

				XPathHelper helper = new XPathHelper(doc.getDocumentElement());

				List<String> currValue = null;
				String xPathExp = RESOURCE_PROFILE_BODY + "/RestrictedPortletURL/text()";
				currValue = helper.evaluate(xPathExp);
				if (currValue != null && currValue.size() > 0) {
					profile.setRestrictedPortletURL(currValue.get(0));
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);

				xPathExp = RESOURCE_PROFILE_BODY + "/OpenPortletURL/text()";
				currValue = helper.evaluate(xPathExp);
				if (currValue != null && currValue.size() > 0) {
					profile.setOpenPortletURL(currValue.get(0));
				} else
					throw new Exception("I'm not able to read the path: " + xPathExp);

				LOG.info("returning: " + profile);
				return profile;
			}

		} catch (Exception e) {
			LOG.error("Error while trying to read the  SecondaryType " + SECONDARY_TYPE + " with appID " + appID
					+ " from scope " + scope, e);
			return null;
		} finally {

		}

	}

	/**
	 * Gets the geoportal config app profile.
	 *
	 * @return the geoportal config app profile
	 */
	public GeoportalConfigApplicationProfile getGeoportalConfigAppProfile() {
		return geoportalConfigAppProfile;
	}

	/**
	 * Gets the gcube generic query string.
	 *
	 * @param secondaryType the secondary type
	 * @param appId         the app id
	 * @return the gcube generic query string
	 */
	public static String getGcubeGenericQueryString(String secondaryType, String appId) {

		return "for $profile in collection('/db/Profiles/GenericResource')//Resource "
				+ "where $profile/Profile/SecondaryType/string() eq '" + secondaryType
				+ "' and  $profile/Profile/Body/AppId/string() " + " eq '" + appId + "'" + "return $profile";
	}

	/**
	 * Gets the secondary type.
	 *
	 * @return the secondary type
	 */
	public String getSecondaryType() {
		return secondaryType;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoportalConfigApplicationProfileReader [secondaryType=");
		builder.append(secondaryType);
		builder.append(", scope=");
		builder.append(scope);
		builder.append(", appID=");
		builder.append(appID);
		builder.append(", geoportalConfigAppProfile=");
		builder.append(geoportalConfigAppProfile);
		builder.append("]");
		return builder.toString();
	}

}
