package org.gcube.datatransfer.resolver.geoportal.exporter;

import lombok.Data;

@Data
public class GeoportalJsonResponse {
	
	//State can be "OK" or "ERROR"
	String state;
	String url;
	String message;
}
