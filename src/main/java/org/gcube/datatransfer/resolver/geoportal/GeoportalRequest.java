package org.gcube.datatransfer.resolver.geoportal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;


/**
 * The Class GeoportalRequest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Mar 23, 2023
 */
@Slf4j
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoportalRequest {

	public static final String P_GCUBE_SCOPE = "gcube_scope";
	public static final String P_ITEM_TYPE = "item_type";
	public static final String P_ITEM_ID = "item_id";
	public static final String P_RESOLVES_AS = "res";
	public static final String P_QUERY_STRING = "query_string";

	@JsonProperty(P_GCUBE_SCOPE)
	private String gcubeScope;
	/**
	 * It is the UCD ID {usecase_id}
	 */
	@JsonProperty(P_ITEM_TYPE)
	private String itemType;
	/**
	 * It is the Project ID {project_id}
	 */
	@JsonProperty(P_ITEM_ID)
	private String itemID;
	
	@JsonProperty(P_QUERY_STRING)
	private String queryString;
	
	@JsonProperty(P_RESOLVES_AS)
	private String res;

}
