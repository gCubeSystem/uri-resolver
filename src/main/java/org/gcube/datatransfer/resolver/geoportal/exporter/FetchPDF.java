package org.gcube.datatransfer.resolver.geoportal.exporter;

import org.gcube.application.geoportaldatamapper.shared.FileReference;

public class FetchPDF {

	private final int MAX_RETRY = 60;
	private FileReference fileRef;
	private String code;

	private int attempt = 0;

	public FetchPDF() {

	}

	public FetchPDF(FileReference fileRef, String code, int attempt) {
		super();
		this.fileRef = fileRef;
		this.code = code;
		this.attempt = attempt;
	}

	public int getMAX_RETRY() {
		return MAX_RETRY;
	}

	public FileReference getFileRef() {
		return fileRef;
	}

	public String getCode() {
		return code;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setFileRef(FileReference fileRef) {
		this.fileRef = fileRef;
	}
	
	public synchronized int incrementAttempt() {
		attempt++;
		return attempt;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FetchPDF [MAX_RETRY=");
		builder.append(MAX_RETRY);
		builder.append(", fileRef=");
		builder.append(fileRef);
		builder.append(", code=");
		builder.append(code);
		builder.append(", attempt=");
		builder.append(attempt);
		builder.append("]");
		return builder.toString();
	}


}
