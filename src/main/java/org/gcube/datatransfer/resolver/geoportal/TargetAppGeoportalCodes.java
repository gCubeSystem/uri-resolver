/**
 *
 */
package org.gcube.datatransfer.resolver.geoportal;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.gcube.datatransfer.resolver.services.GeoportalResolver;

/**
 * The Enum TargetAppGeoportalCodes.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 24, 2023
 */
public enum TargetAppGeoportalCodes {

	GEO_DV(GeoportalResolver.GEO_DV, "data-viewer", "Geoportal Viewer"),
	GEO_DE(GeoportalResolver.GEO_DE, "data-entry", "Geoportal Entry");

	private String id; // the code id
	private String name; // the code value
	private String description;

	/**
	 * Instantiates a new resource catalogue codes.
	 *
	 * @param id          the id
	 * @param name        the target app
	 * @param description the description
	 */
	private TargetAppGeoportalCodes(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {

		return id;
	}

	/**
	 * Gets the target app.
	 *
	 * @return the target app
	 */
	public String getTarget_app() {
		return name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {

		return description;
	}

	/**
	 * Codes.
	 *
	 * @return the list
	 */
	public static List<String> codes() {

		return Arrays.asList(TargetAppGeoportalCodes.values()).stream().map(TargetAppGeoportalCodes::getId)
				.collect(Collectors.toList());
	}

	/**
	 * Value of id.
	 *
	 * @param id the id
	 * @return the target app geoportal codes
	 */
	public static TargetAppGeoportalCodes valueOfId(String id) {
		if (id == null || id.isEmpty())
			return null;

		List<TargetAppGeoportalCodes> codes = Arrays.asList(TargetAppGeoportalCodes.values()).stream()
				.filter(value -> value.getId().compareTo(id) == 0).collect(Collectors.toList());

		if (codes == null || codes.isEmpty())
			return null;

		return codes.get(0);

	}

	/**
	 * Value of name.
	 *
	 * @param name the name
	 * @return the target app geoportal codes
	 */
	public static TargetAppGeoportalCodes valueOfName(String name) {
		if (name == null || name.isEmpty())
			return null;

		List<TargetAppGeoportalCodes> codes = Arrays.asList(TargetAppGeoportalCodes.values()).stream()
				.filter(value -> value.getTarget_app().compareTo(name) == 0).collect(Collectors.toList());

		if (codes == null || codes.isEmpty())
			return null;

		return codes.get(0);

	}

}
