<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <style>
      html, body {
        margin: 10px;
        width: 100%;
        height: 100%;
        display: table;
      }
      #content {
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
      }
      #title-geo {
        font-weight: bold;
        color: black;
        font-size: 26px;
      }
      #message {
        color: gray;
        font-size: 24px;
      }
    </style>
    <script type="text/javascript">
      async function fetchPDF_URL() {
	    try {
	      const req_headers = {
	        'Content-Type': 'application/json'
	      };
	      <#if geoportalexportprj?has_content>
	      req_headers['geoportalexportprj'] = '${geoportalexportprj}';
	      </#if>
	
	      const response = await fetch('${viewPdfURL}', {
	        method: 'GET',
	        headers: req_headers
	      });
	      return response.json();
	    } catch (error) {
	      showError("Error when exporting PDF :-(");
	      console.error("error: " + error);
	    }
      }
      async function playPDFPoll() {
        const response_object = await fetchPDF_URL();
        if (response_object && response_object.state === "OK") {
          if (response_object.url) {
            window.location.replace(response_object.url);
          } else {
            setTimeout(playPDFPoll, 1000);
          }
        } else {
          showError(response_object?.message || "Error when exporting PDF :-(");
        }
      }
      function showError(error_msg) {
        document.getElementById("inner-content").innerHTML = error_msg;
      }
    </script>
    <title>D4Science Geoportal - Action</title>
  </head>
  <body onload="playPDFPoll()">
    <img alt="D4Science Logo" src="https://services.d4science.org/image/layout_set_logo?img_id=32727"><br />
    <div id="content">
      <p id="title-geo">Geoportal</p><br />
      <div id="inner-content">
        <p style="font-size: 18px;">${action}</p>
        <#if waiting>
          <img alt="D4Science Geoportal Loading Icon" src="https://data.d4science.org/shub/E_a2Y1N2ZIaUhCVlE5R0JXNjJhVXVsTlNyNVJta2ZKZVFMaG52N2gxZm05VWJOb2RFTVBpa0pyV1hsUlg2WXJSTw=="><br />
        </#if>
        <br />
        <p id="message">${message}</p>
      </div>
    </div>
  </body>
</html>
