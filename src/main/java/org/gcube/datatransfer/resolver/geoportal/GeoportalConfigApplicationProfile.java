package org.gcube.datatransfer.resolver.geoportal;

import java.io.Serializable;

/**
 * The Class GeoportalConfigApplicationProfile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 3, 2024
 */
public class GeoportalConfigApplicationProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8340275391003882511L;
	private String restrictedPortletURL;
	private String openPortletURL;

	/**
	 * Instantiates a new geo na data viewer profile.
	 */
	public GeoportalConfigApplicationProfile() {

	}

	/**
	 * Gets the restricted portlet URL.
	 *
	 * @return the restricted portlet URL
	 */
	public String getRestrictedPortletURL() {
		return restrictedPortletURL;
	}

	/**
	 * Sets the restricted portlet URL.
	 *
	 * @param restrictedPortletURL the new restricted portlet URL
	 */
	public void setRestrictedPortletURL(String restrictedPortletURL) {
		this.restrictedPortletURL = restrictedPortletURL;
	}

	/**
	 * Gets the open portlet URL.
	 *
	 * @return the open portlet URL
	 */
	public String getOpenPortletURL() {
		return openPortletURL;
	}

	/**
	 * Sets the open portlet URL.
	 *
	 * @param openPortletURL the new open portlet URL
	 */
	public void setOpenPortletURL(String openPortletURL) {
		this.openPortletURL = openPortletURL;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoportalConfigApplicationProfile [restrictedPortletURL=");
		builder.append(restrictedPortletURL);
		builder.append(", openPortletURL=");
		builder.append(openPortletURL);
		builder.append("]");
		return builder.toString();
	}

}
