package org.gcube.datatransfer.resolver.geoportal.exporter;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.StringWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.gcube.datatransfer.resolver.services.GeoportalExporter;

/**
 * Generates HTML messages for the Geoportal Exporter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jan 21, 2025
 */
public class GeoportalHTMLMessageGenerator {

	private static final String ERROR_PAGE_FTL = "error_page.ftl";
	private static final String GEOPORTAL_EXPORTER_WAITING_PAGE_FTL = "geoportal_exporter_waiting_page.ftl";
	private static final Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);

	static {
		cfg.setClassForTemplateLoading(GeoportalHTMLMessageGenerator.class, "./templates");
		cfg.setDefaultEncoding("UTF-8");
	}

	/**
	 * Entity HTML message.
	 *
	 * @param geoportalexport_prj the geoportalexport prj
	 * @param action the action
	 * @param message the message
	 * @param waiting the waiting
	 * @param viewPdfURL the view pdf URL
	 * @return the string
	 */
	public static String entityHTMLMessage(String geoportalexport_prj, String action, String message, boolean waiting,
			String viewPdfURL) {
		try {
			Template template = cfg.getTemplate(GEOPORTAL_EXPORTER_WAITING_PAGE_FTL);
			Map<String, Object> dataModel = new HashMap<>();
			//dataModel.put("GeoportalExporter", GeoportalExporter.class);
			dataModel.put(GeoportalExporter.HEADER_GEOPORTALEXPORT_PRJ, geoportalexport_prj);
			dataModel.put("action", action);
			dataModel.put("message", message);
			dataModel.put("waiting", waiting);
			dataModel.put("viewPdfURL", viewPdfURL);

			StringWriter writer = new StringWriter();
			template.process(dataModel, writer);
			return writer.toString();
		} catch (IOException | TemplateException e) {
			throw new RuntimeException("Error while generating HTML message", e);
		}
	}

	/**
	 * Gets the error page.
	 *
	 * @param action the action
	 * @param message the message
	 * @return the error page
	 */
	public static String getErrorPage(String action, String message) {
		try {
			Template template = cfg.getTemplate(ERROR_PAGE_FTL);
			Map<String, Object> dataModel = new HashMap<>();
			dataModel.put("action", action);
			dataModel.put("message", message);

			StringWriter writer = new StringWriter();
			template.process(dataModel, writer);
			return writer.toString();
		} catch (IOException | TemplateException e) {
			throw new RuntimeException("Error while generating HTML error page", e);
		}
	}
}
