<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <style>
      html, body {
        margin: 10px;
        width: 100%;
        height: 100%;
        display: table;
      }
      #content {
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
      }
      #title-geo {
        font-weight: bold;
        color: black;
        font-size: 26px;
      }
      #message {
        color: gray;
        font-size: 24px;
      }
    </style>
  </head>
  <body>
    <img alt="D4Science Logo" src="https://services.d4science.org/image/layout_set_logo?img_id=32727"><br />
    <div id="content">
      <p id="title-geo">Geoportal Exporter</p><br />
      <p style="font-size: 18px;">${action}</p>
      <br />
      <p id="message">${message}</p>
    </div>
  </body>
</html>
