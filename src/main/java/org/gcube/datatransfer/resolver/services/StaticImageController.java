/**
 *
 */
package org.gcube.datatransfer.resolver.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.IOUtils;
import org.gcube.datatransfer.resolver.services.error.ExceptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class StaticImageController.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 2, 2024
 */
@Path("img")
public class StaticImageController {

	private static Logger logger = LoggerFactory.getLogger(StaticImageController.class);

//	/**
//	 * To doc.
//	 *
//	 * @param req the req
//	 * @return the input stream
//	 * @throws WebApplicationException the web application exception
//	 */
//	@GET
//	@Path("/{any: .*}")
//	@Produces({ "image/png", "image/jpg", "image/gif" })
//	public InputStream toFile(@Context HttpServletRequest req) throws WebApplicationException {
//		logger.info(StaticImageController.class.getSimpleName() + " toFile called");
//
//		String pathInfo = req.getPathInfo();
//		logger.debug("pathInfo {}", pathInfo);
//		try {
//
//			logger.info("going to {}", pathInfo);
//
//			String realPath = req.getServletContext().getRealPath(pathInfo);
//			return new FileInputStream(new File(realPath));
//
//		} catch (Exception e) {
//
//			if (!(e instanceof WebApplicationException)) {
//				// UNEXPECTED EXCEPTION managing it as WebApplicationException
//				String error = pathInfo + " not found. Please, contact the support!";
//				throw ExceptionManager.internalErrorException(req, error, this.getClass(), null);
//			}
//			// ALREADY MANAGED AS WebApplicationException
//			logger.error("Exception:", e);
//			throw (WebApplicationException) e;
//		}
//	}
//	

	/**
	 * To file out.
	 *
	 * @param req the req
	 * @return the streaming output
	 * @throws WebApplicationException the web application exception
	 */
	@GET
	@Path("/{any: .*}")
	@Produces({ "image/png", "image/jpg", "image/gif" })
	public StreamingOutput toFileOut(@Context HttpServletRequest req) throws WebApplicationException {
		logger.info(StaticImageController.class.getSimpleName() + " toFileOut called");
		String pathInfo = req.getPathInfo();
		logger.debug("pathInfo {}", pathInfo);
		try {
			final String realPath = req.getServletContext().getRealPath(pathInfo);
			logger.info("going to {}", pathInfo);
			return new StreamingOutput() {
				@Override
				public void write(OutputStream os) throws IOException, WebApplicationException {
					FileInputStream fileIS = null;
					try {
						fileIS = new FileInputStream(new File(realPath));
						IOUtils.copy(fileIS, os);
					} catch (Exception e) {
						try {
							if(fileIS!=null)
								fileIS.close();
						} catch (Exception e2) {

						}
					}
				}
			};

		} catch (Exception e) {

			if (!(e instanceof WebApplicationException)) {
				// UNEXPECTED EXCEPTION managing it as WebApplicationException
				String error = pathInfo + " not found. Please, contact the support!";
				throw ExceptionManager.internalErrorException(req, error, this.getClass(), null);
			}
			// ALREADY MANAGED AS WebApplicationException
			logger.error("Exception:", e);
			throw (WebApplicationException) e;
		}
	}
}
