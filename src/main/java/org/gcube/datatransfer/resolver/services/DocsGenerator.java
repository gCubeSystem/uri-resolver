/**
 *
 */
package org.gcube.datatransfer.resolver.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

import org.gcube.datatransfer.resolver.services.error.ExceptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DocsGenerator.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 1, 2022
 */
@Path("docs")
public class DocsGenerator {

	private static Logger logger = LoggerFactory.getLogger(DocsGenerator.class);

	/**
	 * To doc.
	 *
	 * @param req the req
	 * @return the input stream
	 * @throws WebApplicationException the web application exception
	 */
	@GET
	@Path("/{any: .*}")
	public InputStream toDoc(@Context HttpServletRequest req) throws WebApplicationException {
		logger.info(DocsGenerator.class.getSimpleName() + " toDoc called");

		String pathInfo = req.getPathInfo();
		logger.debug("pathInfo {}", pathInfo);
		try {

			if (pathInfo.endsWith("/docs/")) {
				pathInfo += "index.html";
			}

			logger.info("going to {}", pathInfo);

			String realPath = req.getServletContext().getRealPath(pathInfo);
			return new FileInputStream(new File(realPath));

		} catch (Exception e) {

			if (!(e instanceof WebApplicationException)) {
				// UNEXPECTED EXCEPTION managing it as WebApplicationException
				String error = pathInfo + " not found. Please, contact the support!";
				throw ExceptionManager.internalErrorException(req, error, this.getClass(), null);
			}
			// ALREADY MANAGED AS WebApplicationException
			logger.error("Exception:", e);
			throw (WebApplicationException) e;
		}
	}
}
