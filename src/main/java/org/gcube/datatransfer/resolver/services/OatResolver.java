package org.gcube.datatransfer.resolver.services;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.services.error.ExceptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class OatResolver.
 * 
 * generates an old authz token from an UMAToken
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Apr 5, 2022
 */
@Path("oat")
public class OatResolver {

	private static Logger LOG = LoggerFactory.getLogger(OatResolver.class);

	/**
	 * Gets the legacy token.
	 *
	 * @param req the req
	 * @return the legacy token
	 * @throws WebApplicationException the web application exception
	 */
	@GET
	@Path("/get")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getLegacyToken(@Context HttpServletRequest req) throws WebApplicationException {
		LOG.info(this.getClass().getSimpleName() + " GET starts...");

		try {

			String scope = ScopeProvider.instance.get();
			LOG.info("ScopeProvider has scope: " + scope);
			String username = AuthorizationProvider.instance.get().getClient().getId();
			UserInfo userInfo = new UserInfo(username, new ArrayList<>());
			String userToken = authorizationService().generateUserToken(userInfo, scope);
			String msgToken = userToken.substring(0, 10) + "_MASKED_TOKEN";
			LOG.info("returning legacy token {} for user {}", msgToken, username);
			return Response.ok(userToken).build();
		} catch (Exception e) {
			LOG.error("Exception:", e);
			
			if (!(e instanceof WebApplicationException)) {
				// UNEXPECTED EXCEPTION managing it as WebApplicationException
				String error = "Error occurred on getting legacy token. Please, contact the support!";
				if (e.getCause() != null)
					error += "\n\nCaused: " + e.getCause().getMessage();
				throw ExceptionManager.internalErrorException(req, error, this.getClass(), null);
			}
			// ALREADY MANAGED AS WebApplicationException
			throw (WebApplicationException) e;
		}
	}

}
