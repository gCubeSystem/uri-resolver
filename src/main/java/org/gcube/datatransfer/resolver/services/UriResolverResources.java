/**
 *
 */

package org.gcube.datatransfer.resolver.services;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.datatransfer.resolver.UriResolverServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * The UriResolverResources. Show the resources as a JSON
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 24, 2022
 */
@Path("resources")
@Singleton
public class UriResolverResources {

	private static Logger log = LoggerFactory.getLogger(UriResolverResources.class);

	/**
	 * Show all.
	 *
	 * @param application the application
	 * @param request     the request
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getServices(@Context Application application, @Context HttpServletRequest request) {
		log.info("Get Services called");

		ObjectNode rootResources = UriResolverServices.getInstance().getListOfResourceNode(application.getClasses());
		return Response.ok().entity(rootResources).build();
	}

}
