/**
 *
 */
package org.gcube.datatransfer.resolver.services;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * The UriResolverInfo get index.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 24, 2022
 */
@Path("info")
public class UriResolverInfo {

	/**
	 * Info.
	 *
	 * @param req the req
	 * @return the input stream
	 * @throws WebApplicationException the web application exception
	 */
	@GET
	@Produces({ MediaType.TEXT_HTML })
	@Path("")
	public InputStream info(@Context HttpServletRequest req) throws WebApplicationException {
		return new UriResolverIndex().index(req);
	}
}
