/**
 *
 */
package org.gcube.datatransfer.resolver.services;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.gcube.datatransfer.resolver.ConstantsResolver;
import org.gcube.datatransfer.resolver.ConstantsResolver.CONTENT_DISPOSITION_VALUE;
import org.gcube.datatransfer.resolver.services.error.ExceptionManager;
import org.gcube.datatransfer.resolver.util.ValidateContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SMPIDResolver is able to give HTTP resolution to storage resource by ID.
 * 
 * See more at 
 * https://gcube.wiki.gcube-system.org/gcube/URI_Resolver#SMP-ID_Resolver
 * 
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 24, 2022
 */
@Path("id")
public class SMPIDResolver {

	/**
	 *
	 */
	private static final String helpURI = "https://wiki.gcube-system.org/gcube/URI_Resolver#SMP-ID_Resolver";

	private static final String SMP_ID = "smp-id";

	private static Logger logger = LoggerFactory.getLogger(SMPIDResolver.class);

	/**
	 * Gets the smpid.
	 *
	 * @param req                the req
	 * @param smpId              the smp id
	 * @param fileName           the file name
	 * @param contentType        the content type
	 * @param contentDisposition the content disposition
	 * @param validation         the validation
	 * @return the smpid
	 * @throws WebApplicationException the web application exception
	 */
	@GET
	@Path("")
	public Response getSMPID(@Context HttpServletRequest req, @QueryParam(SMP_ID) @Nullable String smpId,
			@QueryParam(ConstantsResolver.QUERY_PARAM_FILE_NAME) String fileName,
			@QueryParam(ConstantsResolver.QUERY_PARAM_CONTENT_TYPE) String contentType,
			@QueryParam(ConstantsResolver.QUERY_PARAM_CONTENTDISPOSITION) String contentDisposition,
			@QueryParam(ConstantsResolver.QUERY_PARAM_VALIDATION) boolean validation) throws WebApplicationException {

		logger.info(this.getClass().getSimpleName() + " GET starts...");

		try {
			// Checking mandatory parameter smpId
			if (smpId == null || smpId.isEmpty()) {
				logger.error(SMP_ID + " not found");
				throw ExceptionManager.badRequestException(req, "Missing mandatory parameter " + SMP_ID,
						SMPIDResolver.class, helpURI);
			}

			// Checking the optional parameter "Content-Disposition"
			CONTENT_DISPOSITION_VALUE dispositionValue = CONTENT_DISPOSITION_VALUE.attachment;
			// Validating the Content-Disposition value
			dispositionValue = ValidateContentDisposition.validValue(req, this.getClass(), helpURI, contentDisposition);

			return StorageIDResolver.resolveStorageId(req, smpId, fileName, contentType, dispositionValue, validation);

		} catch (Exception e) {

			if (!(e instanceof WebApplicationException)) {
				// UNEXPECTED EXCEPTION managing it as WebApplicationException
				String error = "Error occurred on resolving the " + SMP_ID + ": " + smpId
						+ ". Please, contact the support!";
				if (e.getCause() != null)
					error += "\n\nCaused: " + e.getCause().getMessage();
				throw ExceptionManager.internalErrorException(req, error, this.getClass(), helpURI);
			}
			// ALREADY MANAGED AS WebApplicationException
			logger.error("Exception:", e);
			throw (WebApplicationException) e;
		}
	}
}
