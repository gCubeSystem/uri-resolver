/**
 *
 */
package org.gcube.datatransfer.resolver.services;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.gcube.datatransfer.resolver.ConstantsResolver;
import org.gcube.datatransfer.resolver.ConstantsResolver.CONTENT_DISPOSITION_VALUE;
import org.gcube.datatransfer.resolver.services.error.ExceptionManager;
import org.gcube.datatransfer.resolver.util.ValidateContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SMPResolver is able to give HTTP resolution to SMP protocol URIs
 * 
 * See more at 
 * https://gcube.wiki.gcube-system.org/gcube/URI_Resolver#SMP_Resolver
 * 
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 24, 2022
 */
@Path("smp")
public class SMPResolver {

	/**
	 *
	 */
	private static final String helpURI = "https://wiki.gcube-system.org/gcube/URI_Resolver#SMP_Resolver";

	private static final String SMP_URI = "smp-uri";

	private static Logger logger = LoggerFactory.getLogger(SMPResolver.class);

	/**
	 * Gets the smpuri.
	 *
	 * @param req                the req
	 * @param smpURI             the smp uri
	 * @param fileName           the file name
	 * @param contentType        the content type
	 * @param contentDisposition the content disposition
	 * @param validation         the validation
	 * @return the smpuri
	 * @throws WebApplicationException the web application exception
	 */
	@GET
	@Path("")
	public Response getSMPURI(@Context HttpServletRequest req, @QueryParam(SMP_URI) @Nullable String smpURI,
			@QueryParam(ConstantsResolver.QUERY_PARAM_FILE_NAME) String fileName,
			@QueryParam(ConstantsResolver.QUERY_PARAM_CONTENT_TYPE) String contentType,
			@QueryParam(ConstantsResolver.QUERY_PARAM_CONTENTDISPOSITION) String contentDisposition,
			@QueryParam(ConstantsResolver.QUERY_PARAM_VALIDATION) boolean validation) throws WebApplicationException {

		logger.info(this.getClass().getSimpleName() + " GET starts...");

		try {
			// Checking mandatory parameter smpURI
			if (smpURI == null || smpURI.isEmpty()) {
				logger.error(SMP_URI + " not found");
				throw ExceptionManager.badRequestException(req, "Missing mandatory parameter " + SMP_URI,
						SMPResolver.class, helpURI);
			}

			// Checking the optional parameter "Content-Disposition"
			CONTENT_DISPOSITION_VALUE dispositionValue = CONTENT_DISPOSITION_VALUE.attachment;
			// Validating the Content-Disposition value
			dispositionValue = ValidateContentDisposition.validValue(req, this.getClass(), helpURI, contentDisposition);

			return StorageIDResolver.resolveStorageId(req, smpURI, fileName, contentType, dispositionValue, validation);

		} catch (Exception e) {

			if (!(e instanceof WebApplicationException)) {
				// UNEXPECTED EXCEPTION managing it as WebApplicationException
				String error = "Error occurred on resolving the smpURI " + smpURI + ". Please, contact the support!";
				throw ExceptionManager.internalErrorException(req, error, this.getClass(), helpURI);
			}
			// ALREADY MANAGED AS WebApplicationException
			logger.error("Exception:", e);
			throw (WebApplicationException) e;
		}

	}
}
