package org.gcube.datatransfer.resolver.services;

import java.util.Map;
import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponse.Status;
import org.eclipse.microprofile.health.Liveness;

/**
 * The Class UriResolverHealthCheck.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 15, 2024
 */
@Path("health")
public class UriResolverHealthCheck implements HealthCheck {

	@GET
	@Path("")
	@Produces({ MediaType.TEXT_HTML, MediaType.APPLICATION_JSON })
	public Response check() {

		HealthCheckResponse hcr = call();
		return Response.ok().entity(hcr).build();
	}

	/**
	 * Call.
	 *
	 * @return the health check response
	 */
	@Liveness
	@Override
	public HealthCheckResponse call() {

		return new HealthCheckResponse("uri-resolver", Status.UP, Optional.empty());
	}
}