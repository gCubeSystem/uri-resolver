package org.gcube.datatransfer.resolver.util;

import javax.servlet.http.HttpServletRequest;

import org.gcube.datatransfer.resolver.ConstantsResolver;
import org.gcube.datatransfer.resolver.ConstantsResolver.CONTENT_DISPOSITION_VALUE;
import org.gcube.datatransfer.resolver.services.error.ExceptionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class ValidateContentDisposition.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 2, 2019
 */
public class ValidateContentDisposition {
	
	public static Logger LOG = LoggerFactory.getLogger(GetResponseRecordFilter.class);
	

	/**
	 * Valid value.
	 *
	 * @param req the req
	 * @param resolverClass the resolver class
	 * @param helpURI the help URI
	 * @param contentDispositionValue the content disposition value
	 * @return the content disposition value
	 */
	public static CONTENT_DISPOSITION_VALUE validValue(HttpServletRequest req,  Class resolverClass, String helpURI, String contentDispositionValue){
		
		CONTENT_DISPOSITION_VALUE toDispositionValue = CONTENT_DISPOSITION_VALUE.attachment;
		//Validating the Content-Disposition value
		if(contentDispositionValue!=null && !contentDispositionValue.isEmpty()) {
			try {
				//It must have a value of: "inline" or "attachement" 
				toDispositionValue = ConstantsResolver.CONTENT_DISPOSITION_VALUE.valueOf(contentDispositionValue);
			}catch (Exception e) {
				String allowedValues = String.format("{%s,%s}", CONTENT_DISPOSITION_VALUE.inline,CONTENT_DISPOSITION_VALUE.attachment);
				String msg = String.format("Wrong parameter: '%s=%s'. Valid '%s' values are: %s",ConstantsResolver.QUERY_PARAM_CONTENTDISPOSITION,contentDispositionValue,ConstantsResolver.QUERY_PARAM_CONTENTDISPOSITION,allowedValues);
				LOG.error(msg);
				throw ExceptionManager.wrongParameterException(req, msg, resolverClass, helpURI);
			}
		}
		
		return toDispositionValue;
		
	}

}
