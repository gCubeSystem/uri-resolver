package org.gcube.datatransfer.resolver.storage;

import org.gcube.contentmanagement.blobstorage.resource.MyFile;


/**
 * The Class StorageMetadataFile.
 * Small java wrapper to {@link MyFile}} and its size
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 1, 2019
 */
public class StorageMetadataFile {

	private MyFile myFile;
	private long size;

	/**
	 * Instantiates a new storage metadata file.
	 *
	 * @param myFile the my file
	 * @param size the size
	 */
	public StorageMetadataFile(MyFile myFile, long size) {
		this.myFile = myFile;
		this.size = size;
	}

	/**
	 * Gets the my file.
	 *
	 * @return the my file
	 */
	public MyFile getMyFile() {
		return myFile;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public long getSize() {
		return size;
	}

	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		if(myFile==null)
			return null;
		
		return myFile.getName();
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mime type
	 */
	public String getMimeType() {
		if(myFile==null)
			return null;
		
		return myFile.getMimeType();
	}
	
	

}
