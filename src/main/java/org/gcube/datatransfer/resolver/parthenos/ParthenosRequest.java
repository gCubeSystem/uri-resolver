/**
 *
 */
package org.gcube.datatransfer.resolver.parthenos;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Instantiates a new parthenos request.
 */
@NoArgsConstructor

/**
 * Instantiates a new parthenos request.
 *
 * @param entity_name the entity name
 */
@AllArgsConstructor

/**
 * Gets the entity name.
 *
 * @return the entity name
 */
@Getter

/**
 * Sets the entity name.
 *
 * @param entity_name the new entity name
 */
@Setter

/**
 * To string.
 *
 * @return the java.lang. string
 */
@ToString
public class ParthenosRequest {

	@JsonProperty(value = "entity_name", required = true)
	private String entity_name;

}
