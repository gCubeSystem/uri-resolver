package org.gcube.datatransfer.resolver.catalogue.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CatalogueStaticConfigurations.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Apr 6, 2020
 */
public class CatalogueStaticConfigurations{
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CatalogueStaticConfigurations.class);
	
	/** The Constant PREFIX_FOR_PORTAL_RELATIVE_URL_TO_CATALOGUE. */
	static final String PREFIX_FOR_PORTAL_RELATIVE_URL_TO_CATALOGUE = "PREFIX_FOR_PORTAL_RELATIVE_URL_TO_CATALOGUE";
	
	/** The Constant BALANCER_AND_CKAN_PREFIXES_USED_FOR_HOSTNAME. */
	static final String BALANCER_AND_CKAN_PREFIXES_USED_FOR_HOSTNAME = "BALANCER_AND_CKAN_PREFIXES_USED_FOR_HOSTNAME";
	
	/** The Constant DATACATALOGUE_URLs_CONFIGURATIONS_PROPERTIES. */
	static final String DATACATALOGUE_URLs_CONFIGURATIONS_PROPERTIES = "datacatalogue_urls_configurations.properties";

	/** The prefix to be used for catalogue URL. */
	private String prefixToBeUsedForCatalogueURL;
	
	/** The prefixes used for ckan hostname. */
	private List<String> prefixesUsedForCkanHostname;
	
	/**
	 * Instantiates a new static configurations.
	 */
	public CatalogueStaticConfigurations() {
		loadCataloguePortalURLsConfigurations();
	}
	
	
	/**
	 * Load catalogue portal URLs configurations.
	 */
	private void loadCataloguePortalURLsConfigurations(){
		Properties prop = new Properties();
		try {
		
			InputStream in = CkanCatalogueConfigurationsReader.class.getResourceAsStream(DATACATALOGUE_URLs_CONFIGURATIONS_PROPERTIES);
			// load a properties file
			prop.load(in);
			
			// get the property value
			String property = prop.getProperty(PREFIX_FOR_PORTAL_RELATIVE_URL_TO_CATALOGUE);
			prefixToBeUsedForCatalogueURL = property;
			
			// get the property value
			property = prop.getProperty(BALANCER_AND_CKAN_PREFIXES_USED_FOR_HOSTNAME);
			if(property.contains(",")) {
				String[] values = property.split(",");
				prefixesUsedForCkanHostname = Arrays.asList(values);
			}else {
				prefixesUsedForCkanHostname = Arrays.asList(property);
			}
					
		} catch (IOException e) {
			LOG.error("An error occurred on read property file: "+DATACATALOGUE_URLs_CONFIGURATIONS_PROPERTIES, e);
		}
	}
	
	
	/**
	 * Extract catalogue name from a ckan or balancer hostname.
	 *
	 * @param privateCKANCatalogueURL the private CKAN catalogue URL
	 * @return the catalogue name
	 * 
	 * e.g. Returns "-bb" for "ckan-bb"
	 */
	private String extractCatalogueName(String privateCKANCatalogueURL) {
		
		String toPrivateCKANCatalogueURL = privateCKANCatalogueURL;
		LOG.debug("Private Catalogue URL is: "+toPrivateCKANCatalogueURL);
		//removing protocol HTTPs or HTTP
		toPrivateCKANCatalogueURL = toPrivateCKANCatalogueURL.replaceFirst("https://", "");
		toPrivateCKANCatalogueURL = toPrivateCKANCatalogueURL.replaceFirst("http://", "");
		LOG.debug("Removed HTTP[s] protocol, now I have: "+toPrivateCKANCatalogueURL);
		for (String prefix :prefixesUsedForCkanHostname) {
			LOG.trace(prefix+ " found as prefix of: "+privateCKANCatalogueURL);
			if(toPrivateCKANCatalogueURL.startsWith(prefix)) {
				toPrivateCKANCatalogueURL = toPrivateCKANCatalogueURL.replaceFirst(prefix, ""); //removing prefix
				String catalogueName = toPrivateCKANCatalogueURL.substring(0,toPrivateCKANCatalogueURL.indexOf(".d4science"));
				LOG.info("Catalogue Name extraction returning value: "+catalogueName);
				return catalogueName;
			}
		}
		
		LOG.info("Catalogue Name extraction returning null");
		return null;
	}
	
	
	/**
	 * Builds the relative URL to public catalogue gateway.
	 *
	 * @param privateCKANCatalogueURL the private CKAN catalogue URL
	 * 
	 * @return the relative URL that must be used for Public Catalogue Gateway
	 */
	public String buildRelativeURLToPublicCatalogueGateway(String privateCKANCatalogueURL) {
		LOG.info("Trying to build the Catalogue Gateway URL using the prefix: "+prefixToBeUsedForCatalogueURL);
		LOG.debug("The prefixes to build the URL for Public Catalogue published at Gateway level are: "+prefixesUsedForCkanHostname);
		String catalogueName = extractCatalogueName(privateCKANCatalogueURL);
		catalogueName = catalogueName == null? "":catalogueName;
		String toNormalizedNameForURL = catalogueName.replaceAll("\\.", "-");
		String publicCatalogueGatewayURL = String.format("%s%s", prefixToBeUsedForCatalogueURL,toNormalizedNameForURL);
		LOG.info("The Catalogue Gateway URL built is: "+publicCatalogueGatewayURL);
		return publicCatalogueGatewayURL;
	}
	
	/**
	 * Gets the prefixes used for ckan hostname.
	 *
	 * @return the prefixes used for ckan hostname
	 */
	public List<String> getPrefixesUsedForCkanHostname() {
		return prefixesUsedForCkanHostname;
	}
	
	/**
	 * Gets the prefix to be used for catalogue URL.
	 *
	 * @return the prefix to be used for catalogue URL
	 */
	public String getPrefixToBeUsedForCatalogueURL() {
		return prefixToBeUsedForCatalogueURL;
	}
}