package org.gcube.datatransfer.resolver.catalogue;

/**
 * The Class CatalogueRequest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 20, 2022
 */
public class CatalogueRequest {

	private String gcube_scope;
	private String entity_context;
	private String entity_name;
	private String query_string;

	/**
	 * Gets the gcube_scope.
	 *
	 * @return the gcube_scope
	 */
	public String getGcube_scope() {
		return gcube_scope;
	}

	/**
	 * Gets the entity_context.
	 *
	 * @return the entity_context
	 */
	public String getEntity_context() {
		return entity_context;
	}

	/**
	 * Gets the entity_name.
	 *
	 * @return the entity_name
	 */
	public String getEntity_name() {
		return entity_name;
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQuery_string() {
		return query_string;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CatalogueRequest [gcube_scope=");
		builder.append(gcube_scope);
		builder.append(", entity_context=");
		builder.append(entity_context);
		builder.append(", entity_name=");
		builder.append(entity_name);
		builder.append(", query_string=");
		builder.append(query_string);
		builder.append("]");
		return builder.toString();
	}

}
