/**
 *
 */
package org.gcube.datatransfer.resolver.catalogue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Enum ResourceCatalogueCodes.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jan 31, 2017
 *
 *         see wiki page:
 *         https://wiki.gcube-system.org/gcube/URI_Resolver#CATALOGUE_Resolver
 */
public enum ResourceCatalogueCodes {

	CTLG("ctlg", "dataset", "Catalogue Product/Dataset"),
	// CTLGP("ctlg-p","product", "Catalogue Product"),
	CTLGD("ctlg-d", "dataset", "Catalogue Dataset"), CTLGO("ctlg-o", "organization", "Catalogue Organization"),
	CTLGG("ctlg-g", "group", "Catalogue Group");

	private String id; // the code id
	private String value; // the code value
	private String description;

	/**
	 * Instantiates a new resource catalogue codes.
	 *
	 * @param id          the id
	 * @param value       the value
	 * @param description the description
	 */
	private ResourceCatalogueCodes(String id, String value, String description) {
		this.id = id;
		this.value = value;
		this.description = description;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {

		return id;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {

		return value;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {

		return description;
	}

	/**
	 * Codes.
	 *
	 * @return the list
	 */
	public static List<String> codes() {

		return Arrays.asList(ResourceCatalogueCodes.values()).stream().map(ResourceCatalogueCodes::getId)
				.collect(Collectors.toList());
	}

	/**
	 * Value of code id.
	 *
	 * @param id the id
	 * @return the resource catalogue codes
	 */
	public static ResourceCatalogueCodes valueOfCodeId(String id) {
		if (id == null || id.isEmpty())
			return null;

		List<ResourceCatalogueCodes> codes = Arrays.asList(ResourceCatalogueCodes.values()).stream()
				.filter(r -> r.getId().compareTo(id) == 0)
				.collect(Collectors.toList());

		if (codes == null || codes.isEmpty())
			return null;

		return codes.get(0);

	}

	/**
	 * Value of code value.
	 *
	 * @param codeValue the code value
	 * @return the resource catalogue codes
	 */
	public static ResourceCatalogueCodes valueOfCodeValue(String codeValue) {
		if (codeValue == null || codeValue.isEmpty())
			return null;

		List<ResourceCatalogueCodes> codes = Arrays.asList(ResourceCatalogueCodes.values()).stream()
				.filter(r -> r.getValue().compareTo(codeValue) == 0)
				.collect(Collectors.toList());

		if (codes == null || codes.isEmpty())
			return null;

		return codes.get(0);
	}
}
