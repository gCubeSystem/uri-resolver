/**
 *
 */
package org.gcube.datatransfer.resolver.catalogue.resource;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.gcube.datacatalogue.utillibrary.server.DataCatalogueRunningCluster.ACCESS_LEVEL_TO_CATALOGUE_PORTLET;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;


// TODO: Auto-generated Javadoc
/**
 * The Class GatewayCKANCatalogueReference.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 12, 2019
 */

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GatewayCKANCatalogueReference implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The scope. */
	private String scope;

	/** The ckan URL. */
	private String ckanURL;
	
	/** The map access URL to catalogue. */
	private Map<ACCESS_LEVEL_TO_CATALOGUE_PORTLET, String> mapAccessURLToCatalogue;
	
	
	/**
	 * Gets the catalogue URL.
	 *
	 * @param accessLevel the access level
	 * @return the catalogue URL
	 */
	public String getCatalogueURL(ACCESS_LEVEL_TO_CATALOGUE_PORTLET accessLevel) {
		
		return mapAccessURLToCatalogue.get(accessLevel);
	}
	
	/**
	 * Sets the catalogue URL.
	 *
	 * @param catalogueURL the catalogue URL
	 * @param accessLevel the access level
	 */
	protected void setCatalogueURL(String catalogueURL, ACCESS_LEVEL_TO_CATALOGUE_PORTLET accessLevel) {
		
		if(mapAccessURLToCatalogue==null)
			mapAccessURLToCatalogue = new HashMap<ACCESS_LEVEL_TO_CATALOGUE_PORTLET, String>();
		
		mapAccessURLToCatalogue.put(accessLevel, catalogueURL);
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getCkanURL() {
		return ckanURL;
	}

	public void setCkanURL(String ckanURL) {
		this.ckanURL = ckanURL;
	}
	
	
	
}
