/**
 *
 */

package org.gcube.datatransfer.resolver.catalogue.resource;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.gcube.resources.discovery.icclient.ICFactory.client;

import java.io.StringReader;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.gcube.common.resources.gcore.utils.XPathHelper;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datacatalogue.utillibrary.jackan.CkanClient;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueImpl;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueRunningCluster.ACCESS_LEVEL_TO_CATALOGUE_PORTLET;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.exceptions.CkanException;
import org.gcube.datatransfer.resolver.applicationprofile.ApplicationProfileNotFoundException;
import org.gcube.datatransfer.resolver.catalogue.endpoint.CatalogueServiceEndpointReader;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.Query;
import org.gcube.resources.discovery.client.queries.impl.QueryBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;


/**
 * The Class CkanCatalogueConfigurationsReader.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Apr 20, 2018
 */
public class CkanCatalogueConfigurationsReader {

	//private static final String PREFIX_FOR_PORTAL_RELATIVE_URL_TO_CATALOGUE = "PREFIX_FOR_PORTAL_RELATIVE_URL_TO_CATALOGUE";
	/** The Constant logger. */
	//private static final String BALANCER_AND_CKAN_PREFIXES_USED_FOR_HOSTNAME = "BALANCER_AND_CKAN_PREFIXES_USED_FOR_HOSTNAME";
	private static final Logger logger = LoggerFactory.getLogger(CkanCatalogueConfigurationsReader.class);
	
	/** The Constant APPLICATION_PROFILE_NAME. */
	public final static String APPLICATION_PROFILE_NAME = "CkanPortlet";
	
	
	/**
	 * Load catalogue end points through the ckan-util-library.
	 *
	 * @return the gateway ckan catalogue reference
	 * @throws Exception the exception
	 */
	public static GatewayCKANCatalogueReference loadCatalogueEndPoints() throws Exception{
		GatewayCKANCatalogueReference links = new GatewayCKANCatalogueReference();
		links.setScope(ScopeProvider.instance.get());

		DataCatalogueImpl catalogueImpl = CatalogueServiceEndpointReader.getDataCatalogueImpl();

		Map<ACCESS_LEVEL_TO_CATALOGUE_PORTLET, String> accessLevelMap = catalogueImpl.getMapAccessURLToCatalogue();
		for (ACCESS_LEVEL_TO_CATALOGUE_PORTLET accessLevel : accessLevelMap.keySet()) {
			links.setCatalogueURL(accessLevelMap.get(accessLevel), accessLevel);
		}
		
		//String publicVREPortletURL = accessLevelMap.get(ACCESS_LEVEL_TO_CATALOGUE_PORTLET.PUBLIC_VRE);
		String privatePortletURL = accessLevelMap.get(ACCESS_LEVEL_TO_CATALOGUE_PORTLET.PRIVATE_VRE);
		String publicGatewayPortletURL = accessLevelMap.get(ACCESS_LEVEL_TO_CATALOGUE_PORTLET.PUBLIC_GATEWAY);
		
		try{
			//If a catalogue gateway portlet URL is not provided/read from GR, building it
			if(publicGatewayPortletURL==null || publicGatewayPortletURL.isEmpty()) {
				//Building the gateway catalogue public URL from private VRE Portlet URL
				URI toURL = new URI(privatePortletURL);
				String publicURL = privatePortletURL.startsWith("https://")?"https://"+toURL.getHost():"http://"+toURL.getHost();
				//It returns the string "catalogue"
				CatalogueStaticConfigurations staticConf = new CatalogueStaticConfigurations();
				//Replacing for example "ckan-bb" with "[PREFIXES-TO-CATALOGUE-URL]-bb" (e.g catalogue-bb)
				String relativeURLWithCatalogueName = staticConf.buildRelativeURLToPublicCatalogueGateway(catalogueImpl.getCatalogueUrl());
				String toGatewayPortletURL = String.format("%s/%s", publicURL, relativeURLWithCatalogueName);
				links.setCatalogueURL(toGatewayPortletURL, ACCESS_LEVEL_TO_CATALOGUE_PORTLET.PUBLIC_GATEWAY);
			}
		}catch(Exception e){
			logger.warn("Erron on creating public catalogue URL from private URL: "+privatePortletURL, e);
		}

		//Getting the CKAN Portlet URL for current scope
		try{
			String ckanPortletURL = catalogueImpl.getCatalogueUrl();
			links.setCkanURL(ckanPortletURL);
		}catch(Exception e){
			logger.warn("Erron on getting CKAN Porlet URL for scope: "+ScopeProvider.instance.get(), e);
		}

		return links;
	}

	
	/**
	 * Retrieve a ckan dataset given its id. The CkanClient is used, without api key. The result is null also when the dataset is private.
	 *
	 * @param datasetIdorName the dataset idor name
	 * @param catalogueURL the catalogue url
	 * @return the dataset
	 * @throws Exception the exception
	 */
	public static CkanDataset getDataset(String datasetIdorName, String catalogueURL) throws Exception{
		logger.info("Performing request GET CKAN dataset with id: " + datasetIdorName);

		// checks
		checkNotNull(datasetIdorName);
		checkArgument(!datasetIdorName.isEmpty());
		try{
			CkanClient client = new CkanClient(catalogueURL);
			return client.getDataset(datasetIdorName);
		}catch(CkanException e){
			logger.info("Getting dataset "+datasetIdorName+" thrown a CkanException, returning null", e);
			return null;
		}

	}

	/**
	 * Gets the portlet url for scope from is.
	 *
	 * @return the portlet url for scope from is
	 * @throws Exception the exception
	 */
	protected static String getPortletUrlForScopeFromIS() throws Exception {

		String scope = ScopeProvider.instance.get();
		logger.debug("Trying to fetch applicationProfile profile from the infrastructure for " +
			APPLICATION_PROFILE_NAME + " scope: " + scope);
		try {
			Query q =
				new QueryBox(
					"for $profile in collection('/db/Profiles/GenericResource')//Resource " +
						"where $profile/Profile/SecondaryType/string() eq 'ApplicationProfile' and  $profile/Profile/Name/string() " +
						" eq '" +
						APPLICATION_PROFILE_NAME +
						"'" +
						"return $profile");
			DiscoveryClient<String> client = client();
			List<String> appProfile = client.submit(q);
			if (appProfile == null || appProfile.size() == 0)
				throw new ApplicationProfileNotFoundException(
					"Your applicationProfile is not registered in the infrastructure");
			else {
				String elem = appProfile.get(0);
				DocumentBuilder docBuilder =
					DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Node node = docBuilder.parse(new InputSource(new StringReader(elem))).getDocumentElement();
				XPathHelper helper = new XPathHelper(node);
				List<String> currValue = null;
				currValue =
					helper.evaluate("/Resource/Profile/Body/url/text()");
				if (currValue != null && currValue.size() > 0) {
					logger.debug("CKAN Portlet url found is " + currValue.get(0));
					return currValue.get(0);
				}
			}
		}
		catch (Exception e) {
			throw new Exception("Error while trying to fetch applicationProfile profile for name "+APPLICATION_PROFILE_NAME+"from the infrastructure, using scope: "+scope);
		}

		return null;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		ScopeProvider.instance.set("/gcube/devsec/devVRE");
		try {
			GatewayCKANCatalogueReference links = CkanCatalogueConfigurationsReader.loadCatalogueEndPoints();
			System.out.println(links);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
