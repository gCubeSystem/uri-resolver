/**
 *
 */
package org.gcube.datatransfer.resolver.caches;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datatransfer.resolver.init.UriResolverSmartGearManagerInit;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.DetachedREsClient;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.Gateway;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VO;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;


/**
 * The Class LoadingMapOfDetachedVRE.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Jun 10, 2020
 */
public class LoadingMapOfDetachedVRE {

	private static Logger LOG = LoggerFactory.getLogger(LoadingMapOfDetachedVRE.class);
	private static LoadingCache<String, VRE> vreNameToVRE;

	static{

		CacheLoader<String, VRE> loader = new CacheLoader<String, VRE>(){

			@Override
			public VRE load(String vreName)
				throws Exception {

				LOG.info("Loading the cache for VRE name: {}",vreName);
				VRE theVRE = loadVREObjForVREName(vreName);
				if(theVRE!=null)
					LOG.info("Returning {} with scope {} for the VRE name: {}", VRE.class.getSimpleName(), theVRE.getScope(), vreName);
				else {
					LOG.info("No VRE obj for VRE name {}",vreName);
				}
				
				return theVRE;
			}

		};

		RemovalListener<String, VRE> removalListener = new RemovalListener<String, VRE>() {

			@Override
			public void onRemoval(RemovalNotification<String, VRE> arg0) {

				LOG.debug("cache expired");
			}
		};

		vreNameToVRE = CacheBuilder.newBuilder().maximumSize(300).expireAfterWrite(
			12, TimeUnit.HOURS).removalListener(removalListener).
			build(loader);


		//Populating the cache at init stage
		populateTheCache();
		LOG.info("Pre-Loaded detached VRE Name to VRE Obj cache with: "+vreNameToVRE.asMap().size()+" item/s");
	}


	/**
	 * Populate the cache.
	 */
	private static void populateTheCache(){
		
		String authorizationToken = null;
		
		try{
			//Populating the cache by using the detachedres-library
			LOG.info("Trying to pre-populate the detached VREs cache with mapping (VRE Name, VRE Obj)");
		    ScopeProvider.instance.set(UriResolverSmartGearManagerInit.getRootContextScope());
		  
		    authorizationToken = SecurityTokenProvider.instance.get();
		    
		    if(authorizationToken==null) {
				LOG.info("No token found into {} to instance the {}, so reading it from SG configurations",
						SecurityTokenProvider.class.getSimpleName(), DetachedREsClient.class.getSimpleName());
				
				authorizationToken = UriResolverSmartGearManagerInit.getRootTokenFromSGConfiguration();
				if(authorizationToken!=null)
					SecurityTokenProvider.instance.set(authorizationToken);
		    }
		    
			DetachedREsClient detachedREsClient = new DetachedREsClient();
			DetachedREs detachedREs = detachedREsClient.getDetachedREs();
			
			int totalVREDimissed = 0;
			for (Gateway gateway : detachedREs.getGateways().values()) {
				LOG.trace("Gateway: " + gateway.getName());
				int vreDismissedPerGatew = 0;
				for (VO vo : gateway.getVos().values()) {
					LOG.trace("VO: " + vo.getName());
					for (VRE vre : vo.getVres().values()) {
						if(LOG.isTraceEnabled()) {
							LOG.trace("VRE name: " + vre.getName() + 
									" scope: "+vre.getScope() + 
									" VRE catalogue url: " + vre.getCatalogUrl() + 
									" VRE catalog Portlet URL: "+vre.getCatalogPortletURL());
						}
						try {
							if(vre.getScope()!=null && !vre.getScope().isEmpty()) {
								String vreName = vre.getScope().split("/")[3]; 
								vreNameToVRE.asMap().put(vreName, vre);
								vreDismissedPerGatew++;
							}
						}catch (Exception e) {
							LOG.warn("Error on parsing the scope: "+vre.getScope()+ " skipping it");
							
						}
					}
				}
				
				LOG.debug("VREs dismissed loaded and cached per " +  gateway.getName() + " are: "+vreDismissedPerGatew);
				totalVREDimissed+=vreDismissedPerGatew;
			}
			
			LOG.debug("Total detached VREs are: "+totalVREDimissed);
			
			LOG.info("Cache populated with: ");
			Map<String, VRE> mapOfVreNames = vreNameToVRE.asMap();
			for (String key : mapOfVreNames.keySet()) {
				VRE theDetachedVRE = mapOfVreNames.get(key);
				LOG.info("VRE name {}, VRE obj with scope {}, catalogueURL {}, cataloguePortletURL {}",
						key,
						theDetachedVRE.getScope(), 
						theDetachedVRE.getCatalogUrl(),
						theDetachedVRE.getCatalogPortletURL());
			}

		}catch(Exception e){
			//SILENT
		}finally{
			if(authorizationToken!=null) {
				LOG.info("resetting security token provider");
				SecurityTokenProvider.instance.reset();
			}
				
		}
	}
	
	
	
	/**
	 * Gets the VRE obj for input VRE name.
	 *
	 * @param vreName the vre name
	 * @return the vre
	 * @throws ExecutionException the execution exception
	 */
	public static VRE get(String vreName) throws ExecutionException{

		try {
			return vreNameToVRE.get(vreName);
		}catch (Exception e) {
			LOG.info("Error on getting VRE obj for vreName {}. Is the key {} not found in the cache?", vreName, vreName);
			throw e;
		}
	}

	/**
	 * Load VRE obj for VRE name.
	 *
	 * @param vreName the vre name
	 * @return the vre
	 */
	protected static VRE loadVREObjForVREName(String vreName){

		VRE theVRE = vreNameToVRE.getIfPresent(vreName);

		//THIS CHECK SHOULD NOT BE NEEDED
		if(theVRE==null){
			populateTheCache();
			theVRE = vreNameToVRE.getIfPresent(vreName);
		}

		return theVRE;
	}
	
	
	/**
	 * Gets the cache.
	 *
	 * @return the cache
	 */
	public LoadingCache<String, VRE> getCache(){
		return vreNameToVRE;
	}

}
